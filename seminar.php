
<?php
/**
 * The template to display a single post.
 *
 * Template Name: Seminar
 */

 /**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */

function seminarlist_custom_title($title_parts) {
  $seminarslug = get_query_var('seminarslug');

  $is_single_seminar = false;
if ($seminarslug and $seminarslug != 'all') {
    $is_single_seminar = true;
  }

  $uwterm = get_query_var('uwterm');
  $current_term = null;
    
  if (!$uwterm) {
    $current_term = get_current_uw_term();
  } else {
    $current_term = get_uw_term_bycode($uwterm);
  }

  if ($is_single_seminar) {
    $seminar = aae_seminar_getbyslug($seminarslug);
    $title_parts['title'] = $seminar->title . ' ' . $current_term->shortDescription . ' Schedule';
  } else {
    $title_parts['title'] = $current_term->shortDescription . ' Seminar Schedule';
  }
     
  //$title_parts['title'] = $person->FirstName . ' ' . $person->LastName;
  return $title_parts;
}
add_filter( 'document_title_parts', 'seminarlist_custom_title' );

//custom breadcrumbs for seminar list
function seminarlist_custom_breadcrumb( $title, $id = null ) {

  if ($id===seminar_details_pageid()) {
    $seminarslug = get_query_var('seminarslug');
    $seminar = aae_seminar_getbyslug($seminarslug);
    return $seminar->title;
  } elseif($id===seminar_term_pageid()) {
    $uwterm = get_query_var('uwterm');
    $current_term = null;
      
    if (!$uwterm) {
      $current_term = get_current_uw_term();
    } else {
      $current_term = get_uw_term_bycode($uwterm);
    }      
    
    
    return $current_term->shortDescription;

  } elseif ($id===seminars_pageid()) {
    //all seminars - no term (which means current term)
    return $title;
  }

  //default - return original title
  return $title;
}
add_filter( 'the_title', 'seminarlist_custom_breadcrumb', 10, 2 );


function seminar_custom_links( $link, $post_id ) {
  //only change for term pageid
   

  if ($post_id === seminar_term_pageid()) {
    $uwterm = get_query_var('uwterm');
    $current_term = null;
      
    if (!$uwterm) {
      $current_term = get_current_uw_term();
    } else {
      $current_term = get_uw_term_bycode($uwterm);
    }      
  
    $parent_page = get_post(get_post($post_id)->post_parent);
    return get_permalink($parent_page) . 'all/' . term_link($current_term) . '/';
  }

  return $link;
}
add_filter( 'page_link', 'seminar_custom_links', 10, 2 );

$seminarslug = get_query_var('seminarslug');

$is_single_seminar = false;
if ($seminarslug and $seminarslug != 'all') {
  $is_single_seminar = true;
}

$uwterm = get_query_var('uwterm');
$current_term = null;
  
if (!$uwterm) {
  $current_term = get_current_uw_term();
} else {
  $current_term = get_uw_term_bycode($uwterm);
}


$seminars = [];
if ($is_single_seminar) {
  $seminars = aae_seminar_getbyslug($seminarslug);
} else {

  $seminars = aae_seminars_in_term($current_term->termCode);
}

$previous_term = get_previous_uw_term($current_term->termCode);
$next_term = get_next_uw_term($current_term->termCode);

//creates the nav link for the particular term code
function nav_link($term_code, $seminarslug) {
  if ($seminarslug) {
    return '/events/seminars/' . $seminarslug . '/' . $term_code . '/';     
  } else {
    return '/events/seminars/all/' . $term_code . '/';
  }
  
}



function set_seminar_bodyclass($classes) {
  $classes[] = 'archive';
  $classes[] = 'category';
  return $classes;
}
add_filter( 'body_class', 'set_seminar_bodyclass' );

//event link
function aae_event_link($eventid) {
  return '/events/view/s' . $eventid;
}



get_header(); ?>

<div id="page" class="content single">
	<main id="main" class="site-main">

	<?php if ( site_uses_breadcrumbs() ) { custom_breadcrumbs(); } ?>

  <article class=" page type-page hentry">
<header class="entry-header">
		<h1 class="page-title uw-mini-bar"><?php //echo $is_single_seminar ? $seminars[0]->Title : 'Seminars';?></h1>	
    <h2 class="page-title text-center"><?php echo $current_term->shortDescription;?> Schedule
    </header>

	<div class="entry-content">
    <div class="uw-outer-row row-1 has_text_block default-background"><div class="uw-inner-row"><div class="uw-column one-column">

<?php
if (!$is_single_seminar) {
  foreach($seminars as $seminar) :

    seminar_format($seminar, $current_term);

  endforeach;  //end seminar loop
} else {
  seminar_format($seminars, $current_term);
}

?>

<?php
  if (!$is_single_seminar and count($seminars)==0) {
?>
<article id="post-none" class="post type-post status-publish format-standard hentry">
  <p class="text-center"><em>No seminar events currently scheduled for <?php echo $current_term->shortDescription?></em></p>
</article><?php
  }?>

  </div></div></div><!-- end of uw-outerrow, inner-row, one-column-->
	</div> <!-- end of entry container -->


  </article>
<?php if ($is_single_seminar) {?>
  <p class="text-center">
    <em>For additional information contact:</em><br />
    <?php echo $seminars->organizerFirstName . ' ' . $seminars->organizerLastName;?><br />
    <?php echo hide_email($seminars->organizerEmail);?><br />
    <?php echo $seminars->organizerOfficePhone?>
    </p>
<?php
}
?>
	<nav class="navigation post-navigation" role="navigation">
		<h2 class="screen-reader-text">Seminar navigation</h2>
    <div class="nav-links">
      <div class="nav-previous"><?php
    if ($previous_term->termCode > '1046') {
      //seminars start in Fall 2005 (1052)?>
    
    <a href="<?php echo nav_link(term_link($previous_term), $seminarslug);?>" rel="prev"><span class="show-for-sr">Previous semester:</span>
     <span class="post-title">Previous (<?php echo $previous_term->shortDescription;?>)</span></a><?php
    } //end if
    ?></div>
     <div class="nav-next"><?php
     //only show "next" up until 2 semesters after the "current" one (today)
    if (get_next_uw_term(get_next_uw_term(get_current_uw_term()->termCode)->termCode)->termCode >= $next_term->termCode) {?>
     <a href="<?php echo nav_link(term_link($next_term), $seminarslug);?>" rel="next"><span class="show-for-sr">Next semester:</span> 
     <span class="post-title">Next (<?php echo $next_term->shortDescription;?>)</span></a></div>
     <?php
    }
     ?>
     </div>
	</nav>
</main>
</div>

<?php
/*
* Retrieves seminar events for the specified term and specified seminar, and prints them on the page
*/
function seminar_format($seminar, $current_term) {
$seminar_events = aae_seminar_eventsbyslug_and_term($seminar->slug, $current_term->termCode);
?>
<h2 class="uw-mini-bar"><?php echo $seminar->title?></h2>
<?php
if (count($seminar_events)> 0) :

// Switch to sanity from Wordpress
$wp_timezone = date_default_timezone_get();
date_default_timezone_set('America/Chicago');

$now_plus_8 = new \DateTime();
$now_plus_8->sub(new \DateInterval('PT8H'));  //add 8 hours

$future_events = array_filter($seminar_events, function($v) use($now_plus_8) {
  return (new DateTime($v->startDate) > $now_plus_8);
});

$past_events = array_filter($seminar_events, function($v) use($now_plus_8) {
  return (new DateTime($v->startDate) <= $now_plus_8);
});

// print out future events
seminar_events($future_events);

// separator
if (count($future_events) > 0 && count($past_events) > 0) :
echo '<hr />';
endif;

seminar_events($past_events, TRUE);

// Restore ourselves to Wordpress insanity
date_default_timezone_set($wp_timezone);

else :

?>
<article id="post-none" class="post type-post status-publish format-standard hentry">
  <p class="text-center"><em>No seminar events currently scheduled for <?php echo $current_term->shortDescription?></em></p>
</article>
<?php

endif;
}

/**
 * Loops through the supplied seminar events array and prints out the listing
 */
function seminar_events($seminar_events, $in_past = false) {
  foreach(array_values($seminar_events) as $seminar_event):
    $now_plus_8 = new \DateTime();
    $now_plus_8->sub(new \DateInterval('PT8H'));  //add 8 hours
    
    $start_date = strtotime($seminar_event->startDate);
    ?>
    <article id="post-<?php echo $seminar_event->id;?>" class="post type-post status-publish format-standard has-post-thumbnail hentry<?php echo ($seminar_event->isCancelled) ? ' cancelled' : ''; echo ($in_past) ? ' past' : ''; ?>">
      <header class="entry-header">
        <h2 class="entry-title uw-mini-bar"><?php
        if (!$seminar_event->isCancelled) :?>
        <a href="<?php echo aae_event_link($seminar_event->id);?>" rel="bookmark">
        <?php 
        echo !empty($seminar_event->title) ? $seminar_event->title : $seminar_event->presenterName;
        else :
          echo "*** CANCELLED ***";
        endif;
        if (!$seminar_event->isCancelled): ?>
        </a><?php
        endif;?></h2>					
            <div class="entry-meta row">
              <div class="column small-12 medium-2">
                <div class="aae-seminar-day<?php echo $seminar_event->isDifferentDay ?  ' aae-seminar-day-different' : '' ?>"><?php echo date('l',$start_date)?></div>
                <?php
                if ($seminar_event->isDifferentDay) :?>
                  <div class="aae-seminar-differentday">(different day)</div>
                <?php
                endif;?>
                <div class="aae-seminar-date"><?php echo date('F j',$start_date)?></div>
                <div class="aae-seminar-time"><?php echo date('g:ia',$start_date)?></div>
              </div>
              <div class="column small-12 medium-10"><?php
                  if (!empty($seminar_event->title)): ?>
                  <div class="aae-seminar-presented">Presented by:</div>
                  <div class="aae-seminar-presenter"><?php echo $seminar_event->presenterName;?></div><?php
                  endif;
                  if ($seminar_event->isJobMarket) {?>
                  <div class="aae-seminar-jobmarket">Practice Job Talk</div><?php
                  }?>
                  <div class="aae-seminar-presenterinfo"><?php     if (!empty($seminar_event->presenterDepartment)) {
            echo htmlentities($seminar_event->presenterDepartment);
            if (!empty($seminar_event->presenterInstitution)) {
                echo '<br />';
            }
        }
    
        if (!empty($seminar_event->presenterInstitution)) {
            echo $seminar_event->presenterInstitution . '<br />';
        }?></div>
              </div>
            </div>
          </header>
    
      <div class="entry-content">
          <?php
      if (!empty($seminar_event->Abstract)) :
    ?>	<p>
          <?php if (strlen($seminar_event->abstract) > 300) :
             echo str_truncate($seminar_event->abstract, 300) . '[<a href="' . aae_event_link($seminar_event->id) .'">more</a>]' ;
            else :
              echo $seminar_event->abstract;
          endif;?>
        </p><?php
      endif;
      ?>
      </div>
    
      <footer class="entry-footer">
        <?php 
        
        if (!empty($seminar_event->location)) :
          echo makeLinks($seminar_event->location) . '<br />';
        endif;
        if (!empty($seminar_event->onlineLink)) :
          echo 'Online - ';
          if ($seminar_event->isOnlineLinkSecured) :
            echo '<a href="' . aae_event_securedonline_link($seminar_event->id) . '" target="_blank">Login with UW-Madison NetID to view link</a><br /><br />';
            echo '<em>If you do not have a UW-Madison NetID, you can contact the seminar organizer or consult the <a href="mailto:aae-seminars+subscribe@g-groups.wisc.edu">seminar email announcement</a>.</em>';
          else :
            echo '<a href="' . $seminar_event->onlineLink . '" target="_blank">' . $seminar_event->onlineLink . '</a>';
          endif;
        endif;
        ?>
          </footer>
    </article><?php
    endforeach;  //end event loop
    
}
?>

<?php get_footer(); ?>
