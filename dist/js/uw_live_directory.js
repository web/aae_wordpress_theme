function live_directory(element_id) {
  
  var ld = this;
  var input_field = jQuery(element_id);
  
  ld.init = function() {
    //perform AJAX search on keyup in input field
    input_field.on("keyup",function(){
      search_ldap_json(input_field);
    });

    //is the input have text?  if so, run it right away
    if (input_field.val()) {
      search_ldap_json(input_field);
    }
  };
  
  ld.search_ldap_json = function(el) {
    if ((el.val().length > 3) && (el.val() != 'Enter a name') && (el.val() != 'Enter topic or name')) {
      jQuery.ajax({
        url: "https://api.aae.wisc.edu/directory/uw/live-directory",
        data: "q="+encodeURIComponent(el.val()),
        success: function(data) {
          jQuery("#people").html(data);
        }
      });
    }
  };
  
  ld.init();
  return ld;
  
}
jQuery(document).ready(function() { 
  live_directory("#directory_search");
});