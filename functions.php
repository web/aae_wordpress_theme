

<?php

/**
 * File includes
 *
 * The array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
$aae_includes = array(
	'lib/constants.php',
  'lib/directory.php',     //directory functions
  'lib/instruction.php',     //instruction functions
  'lib/headshot.php',
  'login/login.php',
  'lib/pubs.php',
  'lib/events/events.php',
  'lib/child-theme-hooks.php',
  'lib/posts.php',
  'lib/acf.php',                 // Advanced Custom Forms code
  'lib/oauth2/aae_identity_login.php',    // Identity Server login for API tokens
  'lib/aae-api.php'    // AAE API functions
);

foreach ($aae_includes as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'uw-theme'), $file), E_USER_ERROR);
	}

	require_once $filepath;
}

function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

		// Theme assets.
		if ( !WP_DEBUG ) {
			wp_register_style( 'aae-style', get_stylesheet_directory_uri() . '/style.css' , array( 'uwmadison-style' ), wp_get_theme()->get('Version') );
			wp_enqueue_style( 'aae-style' );
		} else {
			wp_register_style( 'aae-style', get_stylesheet_directory_uri() . '/style.css' , array( 'uwmadison-style' ), wp_get_theme()->get('Version') );
			wp_enqueue_style( 'aae-style' );
		}

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function uwmadison_entry_meta() {
  // Hide category and tag text for pages.
  if ( 'post' === get_post_type() ) {
    // printf( '<span class="byline"><span class="author vcard">%1$s<span class="screen-reader-text">%2$s </span> <a class="url fn n" href="%3$s">%4$s</a></span></span>',
    //   get_avatar( get_the_author_meta( 'user_email' ), $author_avatar_size ),
    //   _x( 'Author', 'Used before post author name.', 'uw-theme' ),
    //   esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
    //   get_the_author()
    // );

    /* translators: used between list items, there is a space after the comma */
    $categories_list = get_the_category_list( esc_html__( ', ', 'uw-theme' ) );
    if ( $categories_list && uwmadison_categorized_blog() ) {
      printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'uw-theme' ) . '</span>', $categories_list ); // WPCS: XSS OK.
    }

    /* translators: used between list items, there is a space after the comma */
    $tags_list = get_the_tag_list( '', esc_html__( ', ', 'uw-theme' ) );
    if ( $tags_list ) {
      printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'uw-theme' ) . '</span>', $tags_list ); // WPCS: XSS OK.
    }
  }

  if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
    echo '<span class="comments-link">';
    comments_popup_link( esc_html__( 'Leave a comment', 'uw-theme' ), esc_html__( '1 Comment', 'uw-theme' ), esc_html__( '% Comments', 'uw-theme' ) );
    echo '</span>';
  }

  edit_post_link(
    sprintf(
      /* translators: %s: Name of current post */
      esc_html__( 'Edit %s', 'uw-theme' ),
      the_title( '<span class="screen-reader-text">"', '"</span>', false )
    ),
    '<span class="edit-link">',
    '</span>'
  );

}

// map custom field group row layout value to human-friendly values 
add_filter( 'uw_theme_row_layout', function($row_layout) { 
  $custom_element_filename_map = [ 
      // "group_64187d8059aea" => "course-list",
      // "group_6418819143600" => "seminars-accordion",
      // "group_6418834764e18" => "op-areas-list",
      // "group_6418848920300" => "jobmarket-list",
      // "group_64188559b30e5" => "research-courses",
      // "group_64188675df392" => "seminars-list",
      // "group_64188809370cc" => "event-detail",
      // "group_641889744e410" => "contact-info",
      // "group_64188ac8af2f0" => "taylorhall-directory"
  ]; 

  if (array_key_exists($row_layout, $custom_element_filename_map)) 
      return $custom_element_filename_map[$row_layout]; 

  return $row_layout; 
});

function makeLinks($str) {
  $reg_exUrl = "/(http|https|ftp|ftps)\\:\\/\\/[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(\\/\\S*)?/";
  $urls = array();
  $urlsToReplace = array();
  if (preg_match_all($reg_exUrl, $str, $urls)) {
      $numOfMatches = count($urls[0]);
      $numOfUrlsToReplace = 0;
      for ($i = 0;$i < $numOfMatches;$i++) {
          $alreadyAdded = false;
          $numOfUrlsToReplace = count($urlsToReplace);
          for ($j = 0;$j < $numOfUrlsToReplace;$j++) {
              if ($urlsToReplace[$j] == $urls[0][$i]) {
                  $alreadyAdded = true;
              }
          }
          if (!$alreadyAdded) {
              array_push($urlsToReplace, $urls[0][$i]);
          }
      }
      $numOfUrlsToReplace = count($urlsToReplace);
      for ($i = 0;$i < $numOfUrlsToReplace;$i++) {
          $str = str_replace($urlsToReplace[$i], '<a href="'.$urlsToReplace[$i].'" target="_blank">' . $urlsToReplace[$i] . '</a> ', $str);
      }
      return $str;
  } else {
      return $str;
  }
}

/**
 * Needs as oauth_client.php errors out when trying to start a session within itself
 * 3/22/2023
 */
function register_my_session()             
{                                          
    if (!session_id()) {                   
        session_start();                   
    }                                      
}                                          
                                           
add_action('init', 'register_my_session'); 

/**
 * looks up a pageID by the template name.  Uses caching
 */
function aae_get_page_id($template_name) {
  $cache_key = 'template_ids';
  $cache_group = 'aae_pageids';
  $cache_time = 10;

  // Retrieve the array of templates/page Ids from cache
  $template_ids = get_transient( $cache_group . '_' . $cache_key);

  // If no cached version exists, retrieve them from the
  // DB, and then cache the array for future use.
  if ( $template_ids === false ) {
    // args for the WP search

    $args = array(
      'posts_per_page'   => -1,
      'offset'           => 0,
      'post_type'        => 'page',
      'post_status'     => 'any',
      'meta_query'	    => array(
        array(
          'key'		      => 'aae_page_id',
          'value'       => '',
          'compare'   	=> '!='
        )
      )     
    );    
    $pages_array = get_posts( $args );
    $template_ids = [];

    // populate $template_ids
    foreach($pages_array as $page) :
      $template_ids[get_field ('aae_page_id', $page->ID)] = $page->ID;
    endforeach;

    // cache it
    set_transient( $cache_group . '_' . $cache_key, $template_ids, $cache_time );
    
  }

  // see if the array has the specified template
  if (array_key_exists($template_name, $template_ids)) :
    $pageID = $template_ids[$template_name];
  else :
    $pageID = 1;
  endif;
  
  return $pageID;
}

//obfuscates email addresses
function hide_email($email)
{ $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
  $key = str_shuffle($character_set); $cipher_text = ''; $id = 'e'.rand(1,999999999);
  for ($i=0;$i<strlen($email);$i+=1) $cipher_text.= $key[strpos($character_set,$email[$i])];
  $script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";';
  $script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
  $script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"';
  $script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")"; 
  $script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';
  return '<span id="'.$id.'">[javascript protected email address]</span>'.$script;
}

//truncates string
function str_truncate($text, $chars, $include_ellipse=true) {
  $text = $text . ' ';
  $text = substr($text,0,$chars);
  $text = substr($text,0,strrpos($text,' '));
  if ($include_ellipse) :
    $text = $text . '...';
  endif;
  return $text;
}

//method to be used when a code action should result in the 404 page showing
function force_404() {
  global $wp_query;
  $wp_query->set_404();
  status_header(404);
  include( get_query_template( '404' ) );
  exit; # so that the normal page isn't loaded after the 404 page
}


//adds any custom rewrite tags (for querystring)
function custom_rewrite_tag() {
  //for people detail
  add_rewrite_tag('%netid%', '([^&]+)');

  //for seminar detail
  add_rewrite_tag('%seminarslug%', '([^&]+)');

  //for uw terms
  add_rewrite_tag('%uwterm%', '([^&]+)');

  //event detail
  add_rewrite_tag('%eventid%', '([^&]+)');

  //webpubs listing
  add_rewrite_tag('%pubtypeslug%', '([^&]+)');

    //webpubs title
  add_rewrite_tag('%title%', '([^&]+)');

  // full name - should be in form:  firstname-lastname
  add_rewrite_tag('%fullname%', '([^&]+)');


  //add_rewrite_tag('%variety%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag', 10, 0);

//adds any custom rewrite rules
function custom_rewrite_rule() {
  //!!!! BE SURE TO SAVE PERMALINKS AFTER ALTERING THIS FUNCTION!!!!!!!!

  //faculty details
  add_rewrite_rule('^faculty/(.*)/?','index.php?page_id='. faculty_details_pageid() . '&netid=$matches[1]','top');

  //staff details
  add_rewrite_rule('^staff/(.*)/?','index.php?page_id='. staff_details_pageid() . '&netid=$matches[1]','top');

  //grad student details
  add_rewrite_rule('^grad-students/(.*)/?','index.php?page_id='. gradstudent_details_pageid() . '&netid=$matches[1]','top');

  //emeriti details
  add_rewrite_rule('^emeriti/(.*)/?','index.php?page_id='. emeritus_details_pageid() . '&netid=$matches[1]','top');

  //other person details ("weird" people)
  add_rewrite_rule('^people/(?!job-market-candidates$)(.*)/?','index.php?page_id='. otherperson_details_pageid() . '&netid=$matches[1]','top');

    //all seminars
  add_rewrite_rule('^events/seminars/all/([^/.]+)','index.php?page_id='. seminar_term_pageid() . '&seminarslug=all&uwterm=$matches[1]','top');

  //specific seminar, maybee specifying term
  add_rewrite_rule('^events/seminars/([^/.]+)/?([^/.]+)?/?','index.php?page_id='. seminars_pageid() . '&seminarslug=$matches[1]&uwterm=$matches[2]','top');

  //graduates for each semester
  add_rewrite_rule('^graduates/([^/]+)/?$','index.php?page_id='. graduate_list_pageid() . '&uwterm=$matches[1]','top');

  //single graduate detail for a particular semester - uses term and name both
  add_rewrite_rule('^graduates/([^/]+)/([^/]+)/?','index.php?page_id='. graduate_detail_pageid() . '&uwterm=$matches[1]&fullname=$matches[2]','top');

  // ADDED 9/19/2024 for Fall 2024 only (hopefully)
  //scholars for each semester
  add_rewrite_rule('^scholars/([^/]+)/?$','index.php?page_id='. scholar_list_pageid() . '&uwterm=$matches[1]','top');

  //single scholars detail for a particular semester - uses term and name both
  add_rewrite_rule('^scholars/([^/]+)/([^/]+)/?','index.php?page_id='. scholar_detail_pageid() . '&uwterm=$matches[1]&fullname=$matches[2]','top');

  // REMOVED when pubs archived offline - 2/4/2022
  //webpubs detail
  //add_rewrite_rule('^pubs/([^/.]+)/([^/.]+)','index.php?page_id='. webpubs_detail_pageid() . '&pubtypeslug=$matches[1]&title=$matches[2]','top');

  //webpubs listing (type)
  //add_rewrite_rule('^pubs/([^/.]+)?','index.php?page_id='. webpubs_listing_pageid() . '&pubtypeslug=$matches[1]','top');
  //add_rewrite_rule('^pubs/((?!.*(misc|status)).*)/?','index.php?page_id='. webpubs_listing_pageid() . '&pubtypeslug=$matches[1]','top');

  //event detail
  add_rewrite_rule('^events/view/([^/.]+)?','index.php?page_id='. event_details_pageid() . '&eventid=$matches[1]','top');
}
add_action('init', 'custom_rewrite_rule', 10, 0);

