<?php
/**
 * The template to display search results.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package UW Theme
 */


parse_str($_SERVER['QUERY_STRING'], $get_array);
$search_string =  esc_html($get_array['s']);

get_header(); ?>

    <div id="page" class="content page-builder">
        <main id="main" class="site-main">
            <?php if ( site_uses_breadcrumbs() ) { custom_breadcrumbs(); } ?>
      <article class="page">
            <header class="page-header">
                <h1 class="page-title uw-mini-bar">Search</h1>
            </header><!-- .page-header -->
            <div class="uw-outer-row">
                <div class="uw-inner-row uw-gcse">
                    <?php
                    if ( site_uses_google_search() ) {?>
                    <div class="uw-gsc-search-input">
                        <script>
                    (function() {
                      var cx = '<?php echo gcse_id(); ?>';
                      var gcse = document.createElement('script');
                      gcse.type = 'text/javascript';
                      gcse.async = true;
                      gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                      var s = document.getElementsByTagName('script')[0];
                      s.parentNode.insertBefore(gcse, s);
                    })();
                        </script>
                        <div>
                            <gcse:searchbox></gcse:searchbox>
                        </div>
                    </div><?php
                    } else {?>
                    <div class="uw-wp-search-input">
                        <?php get_search_form();?>
                    </div>
                    <?php
                    }?>
                </div>
                <div class="uw-inner-row">
                    <div class="uw-column wide-column uw-gcse">
                        <?php
                        if ( site_uses_google_search() ) {
                        get_template_part( 'content-parts/content', 'search-gcse' );
                        } else {
                        get_template_part( 'content-parts/content', 'search-wp' );
                        }
                        ?>
                    </div>

                    <div class="uw-column narrow-column uw-content-box directory-results">
                        <div class="uw-pe uw-pe-text_block">
                            <h2><a name="dir_results"></a>Directory matches</h2>
                            <form action="//wisc.edu/search/" method="get" _lpchecked="1">
                                <div>
                                    <label for="directory_search">Refine directory search:</label> <input value="<?php echo $search_string?>" id="directory_search" name="q" autocomplete="off" aria-controls="people" type="text">
                                </div>
                            </form>
                            <p class="small-text text-right"><i><a href="//wisc.edu/directories/#tips">Directory help</a></i></p>
                            <div id="people" aria-live="polite" aria-atomic="true" role="region">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
      </article>
    </div>

<?php get_footer(); ?>



<script type="text/javascript" src="<?php  echo get_stylesheet_directory_uri();?>/dist/js/uw_live_directory.js"></script>

  <script>

    // instantiate second live_directory instance for watching the GSCE input and submit
    jQuery(document).ready(function() { 
	  
      var gcse_input = "#gsc-i-id1",
          gsce_live_directory = live_directory(gcse_input);

      var watchEnterKey = function(event) {

        var key = event.keyCode || event.which;


        if (key === 13) {
			
          event.preventDefault();
          directoryLookup();
        }
        return false;
      }


      var directoryLookup = function() {
		  
        jQuery("#directory_search").val(jQuery(gcse_input).val());
        gsce_live_directory.search_ldap_json(jQuery(gcse_input));
      }

      jQuery(".uw-gsc-search-input").on("keyup", ".gsc-input", watchEnterKey);
      jQuery(".uw-gsc-search-input").on("click", "input.gsc-search-button", directoryLookup);

    });
  </script>