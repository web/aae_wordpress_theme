
<?php
/**
 * The template to display a single post.
 *
 * Template Name: Graduate Detail
 */

 /**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */


$fullname = get_query_var('fullname');
$graduate_listing_term = get_uw_term_bycode(get_query_var('uwterm'));
$graduate = graduate_detail($graduate_listing_term, $fullname);

//if a user object wasn't returned, this graudate doesn't exist - 404
if (empty($graduate)) {
  force_404();
}

function graduate_custom_title($title_parts) {
    $fullname = get_query_var('fullname');
    $graduate_listing_term = get_uw_term_bycode(get_query_var('uwterm'));
    $graduate = graduate_detail($graduate_listing_term, $fullname);
     $title_parts['title'] = $graduate->firstName . ' ' . $graduate->lastName . ' | ' . $graduate_listing_term->shortDescription . ' Graduates';

    return $title_parts;
}
add_filter( 'document_title_parts', 'graduate_custom_title' );

// title of the graduate's breadcrumb
function graduate_custom_breadcrumb( $title, $id = null ) {
  if ($id == graduate_detail_pageid()) :
    $fullname = get_query_var('fullname');
    $graduate_listing_term = get_uw_term_bycode(get_query_var('uwterm'));
    $graduate = graduate_detail($graduate_listing_term, $fullname);
    return $graduate->firstName . ' ' . $graduate->lastName;
  endif;

  return $title;
}
add_filter( 'the_title', 'graduate_custom_breadcrumb', 10, 2 );

// title of graduates list breadcrumb
function graduatelist_custom_breadcrumb( $title, $id = null ) {
	if ($id == graduate_list_pageid()) {
		return graduatelist_custom_breadcrumb_actual($title, $id);
	}

	return $title;
}
add_filter( 'the_title', 'graduatelist_custom_breadcrumb', 10, 2 );

get_header(); ?>

<div id="page" class="content page-builder">
	<main id="main" class="site-main">

	<?php if ( site_uses_breadcrumbs() ) { custom_breadcrumbs(); } ?>

  <article class=" page type-page hentry">
<?php
  $firstName = $graduate->firstName;
  $lastName = $graduate->lastName;
  $middleName = $graduate->middleName;


?>

	<div class="entry-content">
    <div class="uw-outer-row row-1 has_text_block default-background"><div class="uw-inner-row"><div class="uw-column one-column">
<div class="faculty-headshot-contact">
  <div class="faculty-contact">
    <h1 class="page-title uw-mini-bar"><?php echo $firstName. ' ' .$middleName. ' ' .$lastName ?></h1>
    <?php echo !empty($graduate->degree) ? '<h2><p>' . $graduate->degree . '</p></h2>' : null; ?>
    <p><?php echo $graduate->phdMajor ?></p>
    <?php

      ?><dl class="faculty-extra"><?php
      // PhD Minor
      if (!empty($graduate->phdMinor)) :?>
        <dt class="faculty-extra-label">Ph.D. Minor</dt>
        <dd class="faculty-extra-value"><?php echo $graduate->phdMinor ?></dd>
        <?php
      endif;

      // PhD Dissertation title
      if (!empty($graduate->phdDissertation)) :?>
        <dt class="faculty-extra-label">Dissertation Title</dt>
        <dd class="faculty-extra-value"><?php echo $graduate->phdDissertation ?></dd>
        <?php
      endif;

      // PhD Advisor
      if (!empty($graduate->phdAdvisor)) :?>
        <dt class="faculty-extra-label">Advisor</dt>
        <dd class="faculty-extra-value"><?php echo $graduate->phdAdvisor ?></dd>
        <?php
      endif;      

      // Additional degree or major
      if (!empty($graduate->degreeSecond)) :?>
        <dt class="faculty-extra-label">Additional Degree/Major</dt>
        <dd class="faculty-extra-value"><?php echo $graduate->degreeSecond ?></dd>
        <?php
      endif;

      // Certificates
      if (!empty($graduate->certificateOne) || !empty($graduate->certificateTwo)) :?>
        <dt class="faculty-extra-label">Certificate<?php echo !empty($graduate->certificateTwo) ? 's' : null;?></dt>
        <dd class="faculty-extra-value"><?php echo $graduate->certificateOne ?></dd>
        <dd class="faculty-extra-value"><?php echo $graduate->certificateTwo ?></dd>
        <?php
      endif;

      // Hometown
      if (!empty($graduate->hometownCity) and !empty($graduate->hometownState)) :?>
        <dt class="faculty-extra-label">Hometown</dt>
        <dd class="faculty-extra-value"><?php echo $graduate->hometownCity . ', ' . $graduate->hometownState; ?></dd>
        <?php
        endif;

      if (!empty($graduate->email) or !empty($graduate->linkedIn) or !empty($graduate->facebook) or !empty($graduate->twitter) or !empty($graduate->personalUrl)) : ?>
      <dt class="faculty-extra-label">Links</dt>
      <dd class="faculty-extra-value graduate-social-flex-list">
        <ul class=""><?php
          // Email
          if (!empty($graduate->email)) :?>
              <li><a href="mailto:<?php echo $graduate->email?>">Email</a></li><?php
          endif;

          // LinkedIn
          if (!empty($graduate->linkedIn)) :?>
            <li><a href="<?php echo $graduate->linkedIn?>">LinkedIn</a></li><?php
          endif;

              // Facebook
          if (!empty($graduate->facebook)) :?>
            <li><a href="<?php echo $graduate->facebook?>">Facebook</a></li><?php
          endif;

          // Twitter
          if (!empty($graduate->twitter)) :?>
            <li><a href="<?php echo $graduate->twitter?>">Twitter</a></li><?php
          endif;

          // Personal Website
          if (!empty($graduate->personalUrl)) :?>
            <li><a href="<?php echo $graduate->personalUrl?>">Personal Website</a></li><?php
          endif;?>
        </ul>
       </dd>

          <?php
        endif;?><!-- end Links -->

      </dl><!-- end of faculty contact -->

  </div>  <!-- end faculty contact-->
  <div class="faculty-headshot">
  <?php
				if ( property_exists($graduate, 'photoUrl') && $graduate->photoUrl != '' ) :

          echo graduate_image_tag($graduate->photoUrl, $graduate->firstName . ' ' . $graduate->lastName, $graduate_listing_term, 'medium', 600);
				else : ?>
					<img class="buckyhead" src="<?php  echo get_stylesheet_directory_uri() . '/dist/img/no-photo.png'; ?>"/>
				<?php endif; ?>
  </div> <!-- end faculty-headshot-->
</div>
<div class="uw-inner-row">
  <div class="equal-column graduate-scholarships">
<?php
      // Scholarships/Grad Support
      if (!empty($graduate->uwScholarships) || !empty($graduate->otherScholarships) || !empty($graduate->uwGradSupport) 
            || !empty($graduate->bsScholarships) || !empty($graduate->phdAwards)) :?>
        <dl class="faculty-extra">
        <dt class="faculty-extra-label">Scholarships</dt>
        <dd class="faculty-extra-value"><?php
            echo $graduate->bsScholarships;
            echo !empty($graduate->bsScholarships) ? '<br />' : '';
            echo $graduate->phdAwards;
            echo !empty($graduate->phdAwards) ? '<br />' : '';            
            echo str_replace("\r\n", "<br />", $graduate->uwScholarships);
            echo !empty($graduate->uwScholarships) ? '<br />' : '';
            echo str_replace("\r\n", "<br />", $graduate->otherScholarships);
            echo !empty($graduate->otherScholarships) ? '<br />' : '';
            echo str_replace("\r\n", "<br />", $graduate->uwGradSupport) ;
            ?></dd></dl>
        <?php
      endif;

?>
  </div>  <!-- end equal-column graduate-scholarships -->
  <div class="equal-column graduate-awards">
<?php

      // Awards
      if (!empty($graduate->awards)) :?>
      <dl class="faculty-extra">
        <dt class="faculty-extra-label">Awards</dt>
        <dd class="faculty-extra-value"><?php echo str_replace("\r\n", "<br />", $graduate->awards) ?></dd>
      </dl>
        <?php
      endif;
?>


  </div>  <!-- end equal-column graduate-awards -->
</div>
<?php
if (property_exists($graduate, 'bio') && !empty($graduate->bio)):?>
<div class="faculty-bio">
<strong>Bio Sketch</strong><br />
  <?php echo str_replace("\r\n", '<br />', $graduate->bio); ?>

</div>
<?php
endif;
if (property_exists($graduate, 'career') && !empty($graduate->career)):?>
  <div class="graduate-career">
    <strong>Career Plans</strong><br />
    <?php echo str_replace("\r\n", '<br />', $graduate->career); ?>
  </div>
  <?php
  endif;
if (property_exists($graduate, 'personalStatement') && !empty($graduate->personalStatement)):?>
  <div class="graduate-personal-statement">
    <strong>Personal Reflection</strong><br />
    <?php echo str_replace("\r\n", '<br />', $graduate->personalStatement); ?>
  </div>
  <?php
  endif;
  if (property_exists($graduate, 'message') && !empty($graduate->message)):?>
    <div class="graduate-message">
      <strong>Message to Fellow Graduates</strong><br />
      <?php echo str_replace("\r\n", '<br />', $graduate->message); ?>
    </div>
</div> <!-- end of faculty-info-container-->
  <?php endif;?>

  </div></div></div><!-- end of uw-outerrow, inner-row, one-column-->
	</div> <!-- end of entry container -->


  </article>
	</main>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>