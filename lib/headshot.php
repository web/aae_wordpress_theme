<?php
/**
 * returns a formatted img tag using HeadshotUriList
*/
function aae_headshot_imgtag($uri_list, $size, $link_original = false, $alt = "") {
  $wp_img_sizes = get_image_sizes();

  $img_size = array();

  if ( is_array( $size ) ) {
    //width and height specified
    $img_size = $size;
  } elseif ( ! empty( $wp_img_sizes[ $size ] ) ) {
    //need to lookup the size via its name
		$img_size = $wp_img_sizes[ $size ];
	}
  // headshot is in format:
  // https://aae-headshots.s3.us-east-2.amazonaws.com/[LastName]_[FirstName]_[PersonID]-[size]w.[ext]
  
  // use the smallest (mobile) for the fallback
  $img = '<img width="' . $img_size['width'] . '" height="' . $img_size['height'] . '"';
  $img .= ' src="'. $uri_list->mobile . '"';
  $img .= ' class="attachment-' . $img_size['width'] . 'x' . $img_size['height'] .' size-' . $img_size['width'] . 'x' . $img_size['height'] . '"';
  $img .= ' alt="' . $alt . '" ';
  

  // loop thru and add everything into src set EXCEPT "original"
  $src_set = '';
  foreach($uri_list as $k => $headshot) :
    if ($k != 'original') {
      // first split on hypens, and pop off the last one to get  -[size]w.[ext]
      $split_by_hypen = explode('-', $headshot);
      $size_suffix = array_pop($split_by_hypen);

      // now split on the "." and pop to remove the extension
      $split_by_period = explode('.', $size_suffix);
      array_pop($split_by_period);

      $src_set != '' ? $src_set .= ', ' : '' ;
      $src_set .= $headshot . ' ' . $split_by_period[0];
    }
  endforeach;

  $img .= ' srcset="' . $src_set . '"';

  $img .= ' sizes="(max-width: ' . $img_size['width'] . 'px) 100vw, ' . $img_size['height'] .'px" />';

  if ($link_original == true) :
    $img = '<a href="' . $uri_list->original . '" target="_blank">' . $img . '</a>';
  endif;
  
  return $img;
}

/**
 * Get size information for all currently-registered image sizes.
 *
 * @global $_wp_additional_image_sizes
 * @uses   get_intermediate_image_sizes()
 * @return array $sizes Data for all currently-registered image sizes.
 */
function get_image_sizes() {
	global $_wp_additional_image_sizes;

	$sizes = array();

	foreach ( get_intermediate_image_sizes() as $_size ) {
		if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
			$sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
			$sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
			$sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
			$sizes[ $_size ] = array(
				'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
				'height' => $_wp_additional_image_sizes[ $_size ]['height'],
				'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
			);
		}
	}

	return $sizes;
}
?>