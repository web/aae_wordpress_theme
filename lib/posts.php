<?php
/**
 * Prints HTML with date information for current post.
 *
 * override UW Theme
 *
 */
function uwmadison_posted_on() {
 $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
  // if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
  //   $time_string = '<time class="updated" datetime="%3$s">%4$s</time>';
  // }

  $time_string = sprintf( $time_string,
    esc_attr( get_the_date( 'c' ) ),
    esc_html( get_the_date() ),
    esc_attr( get_the_modified_date( 'c' ) ),
    esc_html( get_the_modified_date() )
  );

  $posted_on = sprintf(
    esc_html_x( '%s', 'post date', 'uw-theme' ), $time_string
  );

  $byline = sprintf(
    esc_html_x( 'By %s', 'post author', 'uw-theme' ), esc_html( get_the_author() )
  );

  echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
?>