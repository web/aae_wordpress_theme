<?php

// unknown if this is necessary on wordpress itself - erroring in dev
// path was being relative in dev
$tmp_path = ini_get('session.save_path');
$php_ini_path = dirname(php_ini_loaded_file());
$session_save_path = realpath($php_ini_path . '/' . $tmp_path);
ini_set('session.save_path',$session_save_path);
// end of session path issue


require('http.php');
require('oauth_client.php');
/**
 * Retrieves the API access token from the AAE AWS Cognito.  client_credentials and scopes are hardcoded
 * @return string Access token
 */
function aae_get_token_from_identityserver() {
	$client = new oauth_client_class;
	$client->server = 'AAECognito';
	$client->debug = false;
	$client->debug_http = true;
	
	$client->client_id = AAE_API_CLIENTID; $application_line = __LINE__;
	$client->client_secret = AAE_API_CLIENTSECRET;
	
	if(strlen($client->client_id) == 0
	|| strlen($client->client_secret) == 0)
		die('Client id and secret not set on line '.$application_line.
			' ');
	
	$client->scope = 'instruction/readonly directory/readpublic events/readonly';
	if($success = $client->Initialize()) :
		if($success = $client->Process())
		{
			if(strlen($client->authorization_error))
			{
				$client->error = $client->authorization_error;
				$success = false;
			}
			elseif(strlen($client->access_token))
			{
				$access_token = new stdClass();
				$access_token->token = $client->access_token;
				$access_token->expiry = $client->access_token_expiry;

				return $access_token;
			}
		}
		$success = $client->Finalize($success);
	endif;

	if(!$success) :
		// unnecessary - not storing access token in the session
		//$client->ResetAccessToken();
		die('An error occurred while retrieving the API access token: <pre>' . $client->debug_output . '</pre>');
	endif;
}

?>
