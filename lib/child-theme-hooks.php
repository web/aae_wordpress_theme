<?php

//child theme action hook - seminars accordion - only active seminars
function seminars_accordion() {
  get_template_part( 'content-parts/page-elements/aae_seminars_accordion');
}
add_action('seminars_accordion', 'seminars_accordion', 10, 0);

//child theme action hook - operational areas list
function op_areas_list() {
  get_template_part( 'content-parts/page-elements/aae_operational_areas');
}
add_action('op_areas_list', 'op_areas_list', 10, 0);

//child theme action hook - aae contact info (actually just uwmadison_contact_area)
function aae_contact_area() {
  get_template_part( 'content-parts/page-elements/aae_contact_info');
}
add_action('aae_contact_area', 'aae_contact_area', 10, 0);

//child theme action hook - taylor hall directory
function taylorhall_directory() {
  get_template_part( 'content-parts/page-elements/taylor_hall_directory');
}
add_action('taylorhall_directory', 'taylorhall_directory', 10, 0);

//child theme action hook - seminars list
function seminars_list() {
  get_template_part( 'content-parts/page-elements/aae_seminars_listing');
}
add_action('seminars_list', 'seminars_list', 10, 0);

//child theme action hook - event detail
function event_view() {
  get_template_part( 'content-parts/page-elements/aae_event_detail');
}
add_action('event_view', 'event_view', 10, 0);

//child theme action hook - event detail
function research_courses_accordion() {
  get_template_part( 'content-parts/page-elements/aae_research_courses_accordion');
}
add_action('research_courses_accordion', 'research_courses_accordion', 10, 0);

//child theme action hook - event detail
function course_listing() {
  get_template_part( 'content-parts/page-elements/aae_course_listing');
}
add_action('course_listing', 'course_listing', 10, 0);

//child theme action hook - job market listing
function jobmarket_listing() {
  get_template_part( 'content-parts/page-elements/aae_job_market_listing');
}
add_action('jobmarket_listing', 'jobmarket_listing', 10, 0);
?>