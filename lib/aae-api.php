<?php

/**
 * Master AAE API method.  Uses the cache (unless $force_refresh=true) if it can, otherwise calls the original address.
 * @param string $url Url of the API to be called
 * @param string $api_version OPTIONAL API version to use
 * @param int $expire Sets the cache expiration (in seconds).  Default is 5 seconds
 * @param bool $force_refresh Skip the cache and always call the API directly.  Default is false
 * @return stdClass json-decoded object
 */
function aae_api_get($url, $api_version = '', $expire = 5, $force_refresh = false) {
  if ($force_refresh == true)
    return aae_api_call($url, $api_version);

  return aae_api_cache($url, $expire, $api_version);
}

/**
 * calls the AAE API and returns a json-decoded object
 * @return stdClass json-decoded object
 */
function aae_api_call($url,$api_version = '') {
  $access_token = aae_api_get_accesstoken();

  $args = array(
    'timeout'     => 10,
    'sslverify' => false,   // only used for development
    'headers' => array(
      'Authorization' => 'Bearer ' . $access_token->token
    )
  ); 

  // was an API version specified?
  if (!empty($api_version)) {
    $args['headers']['api-version'] = $api_version;
  }

  $result = wp_remote_get($url, $args);
  if (is_wp_error($result)) {
    return "";
  }  else {

    $body = wp_remote_retrieve_body($result);

    return json_decode($body);  

  }

}

/**
 * Retrieves the access token from either Transients API or identity server directly
 * @return stdClass The access token and expiration in an object
 */
function aae_api_get_accesstoken($force_use_server = false) {
  $transient_name = 'aae_api_token';

  if (!$force_use_server)
    if (true === ($access_token = get_transient($transient_name)))
      return $access_token;

  $access_token = aae_get_token_from_identityserver();

  return $access_token;
  
}

/**
 * returns the aae api address (NOT including trailing slash or http schema)
 * @deprecated deprecated since 11/10/2021
 * @return string
 */
function aae_api_address() {
  return aae_api_prod_address();
}

/**
 * returns the aae api production address (NOT including trailing slash or http schema)
 * @return string
 */
function aae_api_prod_address() {
  return "api.aae.wisc.edu";
}

/**
 * returns the aae api dev address (NOT including trailing slash or http schema)
 * @return string
 */
function aae_api_dev_address() {
  return "dev.api.aae.wisc.edu";
}


/**
 * cache for AAE api calls
 * @param string $url Url of the API call
 * @param int $expire Expiration in seconds - default 5 second
 */
function aae_api_cache($url, $expire = 5, $api_version = '') {
    $group = 'aaeapi';
   
    // check the cache first
    $result = get_transient($group . '_' . $url);
    
    if (false === $result || $result == '') {
      $result = aae_api_call($url, $api_version);
      set_transient($group . '_' . $url, $result,$expire);
    }
  
    return $result;
  }
?>

