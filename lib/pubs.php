<?php

/**
 * Master pubs API call
 */
function aae_pubs_api($relative_url, $api_version = '', $expire = 5, $force_refresh = false) {
  $base_url = 'https://' . aae_api_prod_address() . '/pubs/';
  //$base_url = 'https://' . aae_api_dev_address() . '/directory/';
  //$base_url = 'https://localhost:44314/';   

  $absolute_url = $base_url . $relative_url;

  return aae_api_get($absolute_url, $api_version, $expire, $force_refresh);
}

//page id for webpubs_listing page
function webpubs_listing_pageid() {
  return aae_get_page_id('webpubs-listing');
}

//page id for webpubs_detail page
function webpubs_detail_pageid() {
  return aae_get_page_id('webpubs-details');
}

//page id for Publications page
function pubs_pageid() {
  return aae_get_page_id('pubs');
}

//get webpub listing by slug
function aae_webpub_listing_getbyslug($slug) {
    return aae_pubs_api('webpubs/' . $slug);
}

//get webpub type info
function aae_webpubtype_getbyslug($slug) {
    return aae_pubs_api('webpubs/info/' . $slug);
}

//link for web publications PDF download
function aae_webpub_pdflink($pub, $pubtype) {
  return aae_webpub_basedownload_link($pubtype->Slug) . '/' . $pubtype->FilePrefix . $pub->SeriesNo . '.pdf';
}

function aae_webpub_basedownload_link($slug) {
  return '//api.aae.wisc.edu/pubs/pdf/' . $slug;
}

//get webpub 
function aae_webpub_get($title, $slug) {
    return aae_pubs_api('webpubs/' . $slug . '/' . $title);
}

// BREADCRUMBS OVERRIDE

//overrides page title in breadcrumbs for webpubs listing and detail
function webpubs_custom_breadcrumb( $title, $id = null ) {

  $pubtypeslug = get_query_var('pubtypeslug');
  if ($id===webpubs_listing_pageid() and !empty($pubtypeslug)) {
    return webpubs_type_title($pubtypeslug);
  } elseif ($id===webpubs_detail_pageid() and !empty($pubtypeslug)) {
    $title = get_query_var('title');
    $title_query = aae_webpub_titleforquery($title);
    $pub = aae_webpub_get($title_query,$pubtypeslug);
    return str_truncate($pub->Title, 30, true);
  } 
  
  //nothing - return original
  return $title;
  

}
add_filter( 'the_title', 'webpubs_custom_breadcrumb', 10, 2 );

//get the webpubs type title
function webpubs_type_title($pubtypeslug) {
  $pubtype = aae_webpubtype_getbyslug($pubtypeslug);
  $title = $pubtype->Title;
  if ($pubtype->IsArchived) {
    $title = $title . ' (Archived)';
  }
  return $title;
}


// END BREADCRUMBS OVERRIDE

//returns a title ready for use as a URI
function aae_webpub_titleaslink($title) {
    $chars = 35;
    
    // $title = strtolower(preg_replace("/[^0-9a-zA-Z ]/", "", $title));
    // $title = $title . ' ';
    // $title = substr($title,0,$chars);
    // $title = substr($title,0,strrpos($title,' '));
    // $title = str_replace(' ', '-', $title);

  $title = str_truncate($title, $chars, false);
  $title = strtolower(preg_replace("/[^0-9a-zA-Z ]/", "", $title));
  $title = str_replace(' ', '-', $title);
  return $title;
}

//returns a webpub title used for querying
function aae_webpub_titleforquery($title) {
  return preg_replace('/[^a-z0-9.]+/i', '', $title);
}
//returns html for publication
function format_pub_record($pub) {
  $html = '';

  $author       = $pub->Author;
  $title        = $pub->Title;
  $pub_date     = $pub->PublicationDate;
  $web_authors  = $pub->WebAuthors;
  $publication  = $pub->Publication;
  $pub_place    = $pub->PublicationPlace;
  $publisher    = $pub->Publisher;
  $pages        = $pub->Pages;
  $no_pages     = $pub->NumberOfPages;
  $location     = $pub->Location;
  $volume       = $pub->Volume;
  $issue        = $pub->Issue;
  $editor       = $pub->Editors;
  $url          = $pub->WebURL;

  switch($pub->RecordTypeID) :
    case 1:
      //BookChapter
      $html = '<span class="aae-pub aae-pub-bookchapter">' . $author . '. "';
      $html .= !empty($url) ? '<a href="' . $url . '">' : null;
      $html .= $title;
      $html .= !empty($url) ? '</a>' : null;
      $html .= '" in <span class="aae-publication">' . $publication . '</span>,';
      $html .= !empty($editor) ? ' ed. ' . $editor . ', ' : null;
      $html .= !empty($pub_place) ? $pub_place . ':' : null;
      $html .= !empty($publisher) ? $publisher . ',' : null;
      $html .= !empty($pub_date) ? ' ' . $pub_date : null;
      $html .= !empty($pages) ? ' pp.' . $pages : null;
      $html .= '</span>';      
      break;
    
    case 2:
      //Whole Book
      $html = '<span class="aae-pub aae-pub-wholebook">' . $author . '. ';
      $html .= !empty($url) ? '<a href="' . $url . '">' : null;
      $html .= '<span class="aae-title">' . $title . '</span>.';
      $html .= !empty($url) ? '</a>' : null;
      $html .= !empty($pub_place) ? $pub_place . ':' : null;
      $html .= !empty($publisher) ? $publisher . ',' : null;
      $html .= !empty($pub_date) ? ' ' . $pub_date : null;
      $html .= '</span>';           
      break;

    case 3:
      //BookReview

      break;

    case 4:
      //Abstracts

      break;

    case 5:
      //Journals
      $html = '<span class="aae-pub aae-pub-journal">' . $author . '. "';
      $html .= !empty($url) ? '<a href="' . $url . '">' : null;
      $html .= $title;
      $html .= !empty($url) ? '</a>' : null;
      $html .= '" <span class="aae-publication">' . $publication . '</span> ';
      $html .= !empty($volume) ? $volume : null;
      $html .= !empty($issue) ? '(' . $issue . ')' : null;
      $html .= !empty($pages) ? ':' . $pages : null;
      $html .= !empty($no_pages) ? ':' . $no_pages : null;
      $html .= '</span>';
      
      break;

    case 6:
      //Reports Bulletins

      break;

    case 7:
      //Newspapers & Magazines

      break;

    case 8:
      //Disseration or thesis

      break;

    case 9:
      //Conference proceedings

      break;

    case 10:
      //Whole Monograph

      break;

    case 11:
      //Video Recording

      break;

    case 12:
      //Computer program

      break;

    case 13:
      //Department pub

      break;

    case 14:
      //Unpublished works
      $html = '<span class="aae-pub aae-pub-unpubwork">' . $author . '. "';
      $html .= !empty($url) ? '<a href="' . $url . '">' : null;
      $html .= $title;
      $html .= !empty($url) ? '</a>' : null;
      $html .= '" <span class="aae-publication">' . $publication . '</span> ';
      $html .= '</span>';
      break;
    
    case 15:
      //Web page

      break;

    case 16:
      //edited book

      break;

    case 17:
      //monograph section

      break;
  endswitch;

  return $html;
}




?>