<?php
function acf_add_aae_pageid() {

    acf_add_local_field_group(array(
        'key' => 'group_5eb0d0ec093f8',
        'title' => 'AAE',
        'fields' => array(
            array(
                'key' => 'field_5eb0d123fdcbd',
                'label' => 'AAE Page ID',
                'name' => 'aae_page_id',
                'type' => 'text',
                'instructions' => 'A string for identifying a page.	Must be unique.',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
        ),
        'menu_order' => 100,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => 'Fields used by code behind AAE Child Theme',
    ));
}
add_action('acf/init', 'acf_add_aae_pageid');
?>