<?php

if ( !class_exists("AAeEvents") ) {
  class AAeEvents {

    // Cache expiration in seconds
    public $cache_expiration;

    // Define the plugin name for paths
    public $plugin_name;

    /**
     * Constructor function
     * Set the default instance variables
     */
    public function __construct() {

      // Plugin name
      $this->plugin_name = 'aae_events';

      // Default the cache to 30 minutes
      $this->cache_expiration = 60 * 30;
    }

    /**
     * Init our Wordpress stuff
     */
    public function init() {
      // Register the widget class
      //add_action( 'widgets_init', create_function( '', 'register_widget( "AAeEventsWidget" );' ) );
      // A short code wrapper for ::parse()
      add_shortcode( 'aae_events', array( &$this, 'shortCode') );
    }

    /**
     * Get remote data
     *
     * @param $url {string}
     * @return {array}
     *  Returns an array of data or FALSE
     */
    public function getRemote($url, $opts=array()) {
      $opts = $this->sanitizeOpts($opts); // Sanitize the options

        // Build the parsed URL into a url with query params
        $built_url = $this->buildUrl($url, $opts);

        // Define the cache key
        $cache_key = $this->transientKey($built_url);

        // Pull remote data from the cache or fetch it
        if ( ($remote_cache = get_transient($cache_key)) !== FALSE ) {
          $remote_data = $remote_cache;
        }
        else {
          $get = wp_remote_get($built_url);

          if( is_wp_error( $get ) ) {
            return FALSE;
          }
          elseif ( isset($get['response']['code']) && !preg_match('/^(4|5)/', $get['response']['code']) ) {
            $remote_data = json_decode($get['body']);
            set_transient($cache_key, $remote_data, $this->cache_expiration);
          }
          else {
            $remote_data = FALSE;
          }
        }

        if ( $remote_data !== FALSE ) {
          // If the server returned more events than our limit specified, truncate the array
          // manually. Some of the API methods don't yet accept ?limit=
          // if per_page was passed, don't splice the array
          if ( is_array($remote_data) && count($remote_data) > $opts['limit'] && !isset($opts['per_page']) ) {
            array_splice( $remote_data, $opts['limit'] );
          }

          $data = $this->processRemoteData($remote_data, $opts);

          $out = (object) array(
            'timestamp' => time(),
            'data' => $data,
            );
          return $out;
        }
        else {
          return FALSE;
        }
    }

        /**
     * Sanitize the opts array and inject defaults
     *
     * @param $opts {array}
     * @return {array}
     *  Return a sanitized and default injected options array
     */
    private function sanitizeOpts($opts) {
      // Validate the limit
      if ( isset($opts['limit']) && (int) $opts['limit'] < 1 ) {
        unset($opts['limit']);
      }
      else {
        $opts['limit'] = (int) $opts['limit'];
      }

      // Validate per_page
      if ( isset($opts['per_page']) && (int) $opts['per_page'] < 1 ) {
        unset($opts['per_page']);
      }
      elseif ( isset($opts['per_page']) ) {
        $opts['per_page'] = (int) $opts['per_page'];
      }

      // Validate the page
      if ( isset($opts['page']) && (int) $opts['page'] < 1 ) {
        unset($opts['page']);
      }
      elseif ( isset($opts['page']) ) {
        $opts['page'] = (int) $opts['page'];
      }

      // Defaults
      $defaults = array(
        'limit' => 5,
        'per_page' => null,
        'page' => null,
        'title' => 'Events',
        'show_description' => FALSE,
        'source' => 'function',
        'grouped' => FALSE,
        'header_tag' => 'h2',
        );

      // Merge in the defaults
      $opts = array_merge($defaults, $opts);

      return $opts;
    }

    /**
     * Build a unique cache key for the transient API based on a URL (with query arguments)
     * NOTE: The key has to be less than 40 characters
     * MD5 hex hashes are 32 characters
     *
     * @param $url {string}
     * @return {string}
     *  The unique cache key
     */
    private function transientKey($url) {
      // Needs to be less than 40 characters
      // md5() hex hashes are 32 characters
      return "aaee_r" . md5($url);
    }

    /**
     * Re-build a URL from a parseUrl() parsed url
     * Build the query string based on the query options
     * @param $parsed_url {string}
     * @return {string}
     *  Return a full url string
     */
    public function buildUrl($url, $opts=array()) {
      $opts = $this->sanitizeOpts($opts); // Sanitize the options

      // if per_page is set, unset limit
      if ($opts['per_page'])
        unset($opts['limit']);

      // build URL query from possible query options
      $query_opts = array('limit','per_page','page');
      $query = http_build_query(array_intersect_key($opts, array_flip($query_opts)));

      if (!empty($query))
        $query = "?" . $query;

      return $url . $query;
    }

        /**
     * Process the remote JSON encoded string into a date sorted object
     * Deal with the Wordpress timezone insanity here.
     * All date formatting should be done in here so
     * we only have to worry about timezone switching once.
     *
     * @param $data {string}
     * @return {object}
     *  Return a formatted data object for events
     *
     */
    public function processRemoteData($data, $opts=array()) {
      // Sanitize opts
      $opts = $this->sanitizeOpts($opts); // Sanitize the options

      // Init
      $out = array( 'grouped' => array(), 'ungrouped' => array() );

      // Switch to sanity from Wordpress
      $wp_timezone = date_default_timezone_get();
      date_default_timezone_set('America/Chicago');

      foreach ($data as $event) {
        $start_unix = strtotime($event->startDate);
        $end_unix = strtotime($event->endDate);
        $group_by = apply_filters('uwmadison_events_group_by', '%d_%m_%Y');
        $day_stamp = strftime($group_by, $start_unix);

        $e = (object) array(
          'id' => $event->id,
          'title' => $event->title,
          'subtitle' => $event->subtitle,
          'description' => $event->description,
          'cost' => $event->cost,
          'contact_phone' => $event->phone,
          'contact_email' => $event->email,
          'formatted_dates' => $this->parseDateFormats($start_unix),
          'start_timestamp' => $start_unix,
          'end_timestamp' => $end_unix,
          'all_day_event' => $event->allDayEvent,
          'link' => $this->eventLink($event),
          'url' => $event->url,
          'narrative_listing' => $event->narrative_listing,
          'location' => $event->location,
          'uw_map_url' => $event->uw_map_link,
          );

        // Append to grouped and ungrouped output
        $out['ungrouped'][] = $e;
        $out['grouped'][$day_stamp][] = $e;
      }

      // Restore ourselves to Wordpress insanity
      date_default_timezone_set($wp_timezone);

      // Return
      return $out;
    }

        /**
     * Parse a unix timestamp into all of our defined date formats.
     * Date formats are in the instance variable $this->date_formats
     *
     * @param $unix_time {integer}
     *  Unix timestamp
     * @return {array}
     *  Return an array for names with formatted times
     */
    private function parseDateFormats($unix_time) {
      $out = array();
      $date_formats = apply_filters('uwmadison_events_date_formats', $this->dateFormats());
      foreach ($date_formats as $name => $format) {
        $out[$name] = strftime($format, $unix_time);
      }
      return $out;
    }

        /**
     * Return the default array of date format
     * strings for strftime. It should check
     * for Windows to make compatible strftime
     * strings. This array is filterable in Wordpress.
     *
     * @return {array}
     *  Return a keyed array of strftime parsable strings
     */
    private function dateFormats() {
      $formats = array(
        // Used to render the date in each <li> for individual events
        'default' => '<span class="uwmadison_event_date">%m/%d/%y</span>',
        // Used to render the heading for each date group
        'group_header' => '<span class="uwmadison_event_group_date">%b %e</span>',
        // Used to render the date/time next to individual events in the grouped view
        'group_item' => '<span class="uwmadison_event_date">%l:%M %p</span>',
        );

      /**
       * Windows overrides. Windows doesn't have some strftime variables.
       */
      if ($this->isWindows()) {
        // Difference here is the %d rather than %e (space padded)
        $formats['group_header'] = '<span class="uwmadison_event_group_date">%b %d</span>';
        // Difference here is the %I rather than %l (space paddded)
        $formats['group_item'] = '<span class="uwmadison_event_date">%I:%M %p</span>';
      }

      return $formats;
    }

        /**
     * Check for Windows
     *
     * @return {boolean} Are we Windows, or a working OS?
     */
    private function isWindows() {
      return preg_match('/^win/i',PHP_OS);
    }

    public function eventLink($event) {
      $link = '/events/view/' . $event->id;
      return apply_filters('uwmadison_events_event_link', $link, $event);
    }    
  }





}


?>