<?php
/**
* @package AAEEvents
* @version 0.1.0
*/
/*
Plugin Name: AAE Events
Description: A wordpress plugin to interface with the AAE Events API and UW-Madison events calendar (http://today.wisc.edu)
Author: AAE IT Service Center
Version: 0.1.0
*/

// Load the libraries
require_once(dirname(__FILE__) . '/aae_events.class.php');

if ( !function_exists("aae_events_object") ) {
  /**
   * Factory function for the AAeEvents class
   *
   * @return {object}
   *  Returns an instantiated AAEEvents object, runs ->init() the first time
   *
   */
  function &aae_events_object() {
    static $aae_events_saved;

    if ( $aae_events_saved )
      return $aae_events_saved;

    // Instantiate the main library and init Wordpress
    $aae_events = new AAeEvents();
    $aae_events->init();
    $aae_events_saved = $aae_events;

    return $aae_events_saved;
  }
  aae_events_object(); // Run the factory function
}


if ( !function_exists("uwmadison_events_get_remote") ) {
  /**
   * Helper function to pull remote event data
   *
   * @param $url {string}
   * @param $opts {array}
   *
   * @return {object}
   *  Returns an object for the data returned or false
   *
   */

    

  function uwmadison_events_get_remote($url, $opts=array()) {
      //parse the URL to see if you should send it to UW-Madison or AAE
    $domain = parse_url($url, PHP_URL_HOST);
    if ($domain == 'api.aae.wisc.edu') {
        return aae_events_object()->getRemote($url, $opts);
    } elseif ($domain == 'today.wisc.edu') {
        return uwmadison_events_object()->getRemote($url, $opts);
    }
    
  }
}

/**
 * Master events API call
 */
function aae_events_api($relative_url, $api_version = '', $expire = 5, $force_refresh = false) {
  $base_url = 'https://' . aae_api_prod_address() . '/events/';
  //$base_url = 'https://' . aae_api_dev_address() . '/events/';
  //$base_url = 'https://localhost:44348/';   

  $absolute_url = $base_url . $relative_url;

  return aae_api_get($absolute_url, $api_version, $expire, $force_refresh);
}

if ( !function_exists("aae_seminars_get") ) {
  /**
   * Helper function to pull seminars list
   *
   * @return {object}
   *  Returns an object for the data returned or false
   *
   */
  function aae_seminars_get() {
    return aae_events_api('seminars/active', '2.0', 300);
  }
}

  function aae_seminar_get($seminarid) {
    return aae_events_api('seminars/'. $seminarId . '/details', '2.0', 300);
  }

//get seminar info by slug
function aae_seminar_getbyslug($slug) {
    return aae_events_api('seminars/'. $slug . '/details', '2.0', 300);  
  }

//get all seminar events for a term by a date
function aae_seminar_termbydate($date) {
  return aae_events_api('seminars/term?date=' . date('Y-m-d', $date), '2.0', 300);  
}

//get events by slug and term
function aae_seminar_eventsbyslug_and_term($slug, $term_code) {
  return aae_events_api('seminars/' . $slug . '/events/term/'. $term_code, '2.0', 300);  
}

//gets all seminars taht have events in a term
function aae_seminars_in_term($term_code) {
  return aae_events_api('seminars/term/'. $term_code, '2.0', 300);  
}

//gets a seminar event
function aae_seminar_events_get($eventid) {
  return aae_events_api('seminars/events/'. $eventid . '/details', '2.0', 300);  
}

//gets a dept event
  // deprecated with API 2.0 - no department events
  // function aae_dept_events_get($eventid) {
//   return aae_api_cache('events/dept/events/' . $eventid);
// }

  //returns the page ID for the events-->seminars-->detail page
function seminar_details_pageid() {
  return aae_get_page_id('seminar-details');
}

//page id for seminars by term
function seminar_term_pageid() {
  return aae_get_page_id('seminar-term');
}
  //returns the page ID for the events-->seminars page
function seminars_pageid() {
  return aae_get_page_id('seminars');
}

//returns the page ID for the events-->View page
function event_details_pageid() {
  return aae_get_page_id('event-details');
}

function aae_event_securedonline_link($eventid) {
  return 'https://apps.aae.wisc.edu/seminar-secure/' . $eventid;
}
?>