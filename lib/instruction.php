<?php
/**
 * Master instructions API call
 */
function aae_instruction_api($relative_url, $api_version = '', $expire = 5, $force_refresh = false) {
  $base_url = 'https://' . aae_api_prod_address() . '/instruction/';
  //$base_url = 'https://' . aae_api_dev_address() . '/instruction/';
  //$base_url = 'https://localhost:44374/';   

  $absolute_url = $base_url . $relative_url;

  return aae_api_get($absolute_url, $api_version, $expire, $force_refresh);
}

//gets all active courses
function get_courses() {
  return aae_instruction_api('courses/active/fulldetail', '2.0', 300);
}

//get term for specified date
function get_uw_term($objdate) {
  return aae_instruction_api('terms/bydate?date=' . date ('Y-m-d', $objdate), '2.0', 300);
}

/**
 * Returns an InstructionTerm for the specified term_code or term descr
 * @param string $term_info UW term code or short description (1202, 1196) OR (spring2020, fall2019)
 */
function get_uw_term_bycode($term_info) {
  return aae_instruction_api('terms/' . $term_info, '2.0', 300);
}

//get the current term
function get_current_uw_term() {
  return get_uw_term(time());
}

//get the previous term for the specified term code
function get_previous_uw_term($term_code) {
  return aae_instruction_api('terms/' . $term_code . '/prev', '2.0', 300);
}

//get the previous term for the specified term code
function get_next_uw_term($term_code) {
  return aae_instruction_api('terms/' . $term_code . '/next', '2.0', 300);
}

//gets courses for a particular research area
function get_courses_research($topic_id) {
  return aae_instruction_api('courses/research/' . $topic_id . '/fulldetail', '2.0', 300);
}

//returns the link/slug for a term
function term_link($term) {
  return strtolower(preg_replace('/\s+/', '', $term->shortDescription));
}

//gets syllabi for active courses from current back to specified term
function get_syllabi_to_term($term) {
  return aae_instruction_api('syllabus/pastterms/' . $term, '2.0', 300);
}

/**
 * generates a syllabus link from the syllabus object 
 * @deprecated 2.0.0 Uri included with the syllabus object
 */

function syllabus_download_link($syllabus) {
  return '//' . aae_api_address() . '/instruction/syllabus/download/' . $syllabus->SyllabusID . '.pdf';
}
?>