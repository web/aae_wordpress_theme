<?php

/**
 * Master directory API call
 */
function aae_directory_api($relative_url, $api_version = '', $expire = 5, $force_refresh = false) {
  $base_url = 'https://' . aae_api_prod_address() . '/directory/';
  //$base_url = 'https://' . aae_api_dev_address() . '/directory/';
  //$base_url = 'https://localhost:44314/';   
  
  $absolute_url = $base_url . $relative_url;

  return aae_api_get($absolute_url, $api_version, $expire, $force_refresh);
}

/**
 * Retrieves list of people from api.aae.wisc.edu
*/
function get_people($args) {  
  
  if (!empty($args['staff_type'])) {
    return get_people_by_wip_group($args['staff_type']);
  } elseif (!empty($args['research_area'])) {
    return get_people_by_research_area($args['research_area'][0]);
  } elseif (!empty($args['people'])) {
    return get_people_by_uid($args['people']);    
  } else {
    return get_people_by_wip_group('test');
  }

}

// gets people by user IDs
function get_people_by_uid($userIds) {
  $query = "uid=" . implode("&uid=", $userIds); 
  return aae_directory_api('listing/2?' . $query, '2.0', 300);
}

//gets people by research area
function get_people_by_research_area($topic_id) {
  return aae_directory_api('researchareas/' . $topic_id . '/people', '2.0', 300);
}

//gets people by wip_group
function get_people_by_wip_group($staff_type) {
  $group_id = array();
  
  foreach ($staff_type as $stype) :
    switch ($stype) :
      case 'aae_staff':
        array_push($group_id, 2);
        break;
      case 'faculty':
        array_push($group_id, 1);
        break;
      case 'students':
        array_push($group_id, 4);
        break;
      case 'aff_staff':
        array_push($group_id, 3);
        break;  
      case 'emeriti':
        array_push($group_id, 5);
        break;  
      
    endswitch;
  endforeach;
  
  $vars = array('groups' => $group_id);

  $query = http_build_query($vars, "", '&');
  $string = preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $query); //foo=x&foo=y
  
  return aae_directory_api('webgroups/people?' . $string, '2.0', 300);
}
function get_person($username) {
  return aae_directory_api('person/' . $username . '/wordpress', '2.0', 300);
}

// returns all people who have Website selected in their media types
function get_all_people() {
  return aae_directory_api('listing/2', '2.0', 5);
}

//gets web information profile info for the specified user
function get_wip_detail($username) {
  return aae_directory_api('webprofile/' . $username . '/detail', '2.0', 300);
}

//gets grad web information profile info for the specified user
function get_gradwip_detail($username) {
  return aae_directory_api('webprofile/graduatestudent/' . $username . '/detail', '2.0', 300);
}

//returns the link for the people details page
function get_person_link($person) {
  $webgroup_pageid = get_webgroup_pageid($person->webGroup);
  $cache_key = 'webgrouppageid_' . $webgroup_pageid;

  // Retrieve link from cache
  $link = wp_cache_get($cache_key, 'aae_directory' );

  // If no cached version exists, retrieve the link from the
  // DB, and then cache it for future use.
  if ( $link === false ) {
      $link = get_page_link($webgroup_pageid);
        wp_cache_set( $cache_key, $link, 'aae_directory', 10 );
  }
  return $link . $person->netID . "/";
}

//returns the link for the people details page for a job market candidate
function get_jobmarketcandidate_link($person) {
  return get_page_link(get_webgroup_pageid($person->webGroup)) . $person->netID . "/";
}

// returns the link for the graduate details page
function get_graduate_link($graduate, $uwterm) {
  return get_page_link(graduate_list_pageid()) . graduate_link($graduate, $uwterm); 
}

// returns the link for the scholar details page
function get_scholar_link($scholar, $uwterm) {
  return get_page_link(scholar_list_pageid()) . scholar_link($scholar, $uwterm); 
}

//returns the page_id for a particular web group
function get_webgroup_pageid($webGroup) {
  switch (strtolower(preg_replace('/\s+/', '', $webGroup))) {
    case "faculty":
      return faculty_list_pageid();
      break;
    case "administrativestaff":
      return staff_list_pageid();
      break;      
    case "graduatestudents":
      return gradstudent_list_pageid();
      break;
    case "emeritusfaculty":
      return emeritus_list_pageid();
      break;
    case "other":
        return people_list_pageid();
        break;      
  }
}

//returns all active JMC groups
function get_jmc_groups() {
  return aae_directory_api('jobmarket/groups', '2.0', 300);
}

/**
 * Returns all people marked as job market candidates that are approved for public
 */
function get_jobmarket_candidates($groupid) {
  return aae_directory_api('jobmarket/' . $groupid . '/people', '2.0', 300);
}


//returns the web group object that the person belongs to
function get_webgroup_for_person($username) {
  return aae_directory_api('person/' . $username . '/webgroup');
}

//returns whether the specified page id is a details page
function people_is_details_page($page_id) {
  if ($page_id === faculty_details_pageid()) {
    return true;
  }

  if ($page_id === staff_details_pageid()) {
    return true;
  }

  if ($page_id === gradstudent_details_pageid()) {
    return true;
  }

  if ($page_id === emeritus_details_pageid()) {
    return true;
  }

  if ($page_id == otherperson_details_pageid()) {
    return true;
  }

  return false;
}

//returns the page ID for the faculty-->details page
function faculty_details_pageid() {
  return aae_get_page_id('faculty-details');
}

//returns the page ID for the faculty listings page
function faculty_list_pageid() {
  return aae_get_page_id('faculty-listing');
}

//returns the page ID for the staff-->details page
function staff_details_pageid() {
  return aae_get_page_id('staff-details');
}

//returns the page ID for the staff listing page
function staff_list_pageid() {
  return aae_get_page_id('staff-listing');
}

//returns the page ID for the emeritus-->details page
function emeritus_details_pageid() {
  return aae_get_page_id('emeritus-details');
}

//returns the page ID for the emeritus listing page
function emeritus_list_pageid() {
  return aae_get_page_id('emeritus-listing');
}

//returns the page ID for the other-->details page
//only used for people who are "abnormal" - no listing page for them
function otherperson_details_pageid() {
  return aae_get_page_id('otherperson-details');
}

//returns the page ID for the grad student-->details page
function gradstudent_details_pageid() {
  return aae_get_page_id('gradstudent-details');
}

//returns the page ID for the grad student listing page
function gradstudent_list_pageid() {
  return aae_get_page_id('gradstudent-listing');
}

//returns the page ID for the graduates->[uwterm] page
function graduate_list_pageid() {
  return aae_get_page_id('graduate-listing');
}

//returns the page ID for the scholars->[uwterm] page
function scholar_list_pageid() {
  return aae_get_page_id('scholar-listing');
}

//returns the page ID for the people listing page (which is empty - placeholder for "weird" people)
function people_list_pageid() {
  return aae_get_page_id('people');
}

//returns the page ID for the graduates->[uwterm]-->[name] page
function graduate_detail_pageid() {
  return aae_get_page_id('graduate-detail');
}

//returns the page ID for the scholars->[uwterm]-->[name] page
function scholar_detail_pageid() {
  return aae_get_page_id('scholar-detail');
}


// returns the latest listing term code for graduates
function graduate_latest_uwterm_code() {
  // !!!!!!!!!!!!!!!!!!!! NEED TO DO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  return aae_directory_api('graduates/latest-term', '2.0', 60);
}

// returns the listing of graduates for the specified listing UW term
function graduate_listing($uwterm) {
  return aae_directory_api('graduates/' . $uwterm->termCode, '2.0', 300);
}

// returns the detail of the graduate specified by listing UW term and full name
function graduate_detail($uwterm, $fullname) {
  return aae_directory_api('graduates/' . $uwterm->termCode . '/' . $fullname, '2.0', 300);
}

// returns the link url to identity the specified graduate
function graduate_link($graduate, $uwterm) {
  return term_link($uwterm) . '/' . strtolower(preg_replace('/\s+/', '-', ($graduate->firstName . ' ' . $graduate->lastName))) . '/';
}

// returns the latest listing term code for scholar
function scholar_latest_uwterm_code() {
  // !!!!!!!!!!!!!!!!!!!! NEED TO DO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  return aae_directory_api('scholars/latest-term', '2.0', 60);
}

// returns the listing of scholars for the specified listing UW term
function scholar_listing($uwterm) {
  return aae_directory_api('scholars/' . $uwterm->termCode, '2.0', 300);
}

// returns the detail of the scholar specified by listing UW term and full name
function scholar_detail($uwterm, $fullname) {
  return aae_directory_api('scholars/' . $uwterm->termCode . '/' . $fullname, '2.0', 300);
}

// returns the link url to identity the specified scholar
function scholar_link($scholar, $uwterm) {
  return term_link($uwterm) . '/' . strtolower(preg_replace('/\s+/', '-', ($scholar->firstName . ' ' . $scholar->lastName))) . '/';
}
//returns the operational area listing with people
function get_oparea_listing() {
  return aae_directory_api('opareas/people', '2.0', 300);
}

//returns all taylor hall residents
function get_taylorhall() {
  return aae_directory_api('listing/2', '2.0', 300);
}

// **********************************************
// Graduate listings section 
// *****************

// used by both the listing and detail page to put "Spring 2020 Graduates" in the breadcrumb
function graduatelist_custom_breadcrumb_actual( $title, $id = null ) {

  if ($id == graduate_list_pageid()) {

      $uwterm = get_query_var('uwterm');
      $grad_listing_term = null;
      
      if (!$uwterm) {
          $grad_listing_term = get_uw_term_bycode(graduate_latest_uwterm_code());
      } else {
          $grad_listing_term = get_uw_term_bycode($uwterm);
      }
      return $grad_listing_term->shortDescription . ' Graduates';
  }

  return $title;
}

// generates the responsive img tag for the specified file name for graduates
function graduate_image_tag($filename, $name, $term, $dimensions, $fallback_size) {
  $img_size = array();

  $wp_img_sizes = get_image_sizes();

  if ( is_array( $dimensions ) ) {
    //width and height specified
    $img_size = $dimensions;
  } elseif ( ! empty( $wp_img_sizes[ $dimensions ] ) ) {
    //need to lookup the size via its name
		$img_size = $wp_img_sizes[ $dimensions ];
	}

  $img = '<img src="' . graduate_image_url($filename, $term, $fallback_size) . '"';
  $img .= ' srcset="' . graduate_image_url($filename, $term, 320) . ' 320w, ';
  $img .= graduate_image_url($filename, $term, 600) . ' 600w" ';
  $img .= 'width="' . $img_size['width'] . '" height="' . $img_size['height'] . '" ';
  $img .= ' class="attachment-' . $img_size['width'] . 'x' . $img_size['height'] .' size-' . $img_size['width'] . 'x' . $img_size['height'] . '"';
  $img .= ' sizes="(max-width: 600px) 100vw, 600px"';
  $img .= ' alt="' . $name . '" />';
  return $img;
}

function graduate_image_url($filename, $term, $size) {
  $path_parts = pathinfo($filename);
  $term_link = term_link($term);
  return 'https://aae-graduatingstudents.s3.us-east-2.amazonaws.com/' . $term_link . '/' . $path_parts['filename'] . '-' . $size . 'w.' . $path_parts['extension'];
}

// **********************************************
// Scholar listings section 
// *****************

// used by both the listing and detail page to put "Fall 2024 Scholars" in the breadcrumb
function scholarlist_custom_breadcrumb_actual( $title, $id = null ) {

  if ($id == scholar_list_pageid()) {

      $uwterm = get_query_var('uwterm');
      $scholar_listing_term = null;
      
      if (!$uwterm) {
          $scholar_listing_term = get_uw_term_bycode(scholar_latest_uwterm_code());
      } else {
          $scholar_listing_term = get_uw_term_bycode($uwterm);
      }
      return $scholar_listing_term->shortDescription . ' Scholars &amp; Sponsors';
  }

  return $title;
}

abstract class WebGroup 
{
  const Faculty = 1;
  const Staff = 2;
  const GradStudents = 4;
  const Emeriti = 5;
  const Other = 7;
}

/**
 * ACF filter when "Select Individual People" is selected in the faculty/staff listing
 */
function acf_load_select_indpeople( array $field ) : array {

  if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
    $field['choices'] = aae_faculty_as_select();

  }
  $field['choices'] = aae_faculty_as_select();

  return $field;
}
add_filter( "acf/load_field/key=field_65f62e8e781fa", 'acf_load_select_indpeople', 10, 1 );


 /**
  * Returns all faculty as a select list
  */
function aae_faculty_as_select() {
  $people = get_all_people();

  $select = array();
  foreach($people as $person){
    $select[$person->id] = $person->displayName;
  }
  return $select;
}
?>