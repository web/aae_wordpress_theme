<?php
  // Switch timezone temporarily
  $wp_timezone = date_default_timezone_get();
  date_default_timezone_set('America/Chicago');


$eventid = substr(get_query_var('eventid'),1);

$event = aae_seminar_events_get($eventid);

?>
<div class="row">
<?php
if (!empty($event->publicityImage)) :?>
<div class="large-3 column">
<?php
    echo '<img src="' . $event->publicityImage->uri . '" alt="' . $event->presenterName . '" />';
?>
</div> <!-- end of left column -->
<?php
endif;
?>
<div class="large-9 column">
<h2><?php echo !empty($event->title) ? $event->title : 'To Be Announced' ?></h2>

<p>
    <em>Presented by:</em><br>
    <span class="seminar-presenter"><?php echo $event->presenterName . '<br /></span>';
    if ($event->isJobMarket) {
        echo '<span class="seminar-jobmarket">Practice Job Talk</span><br />';
    }
    if (!empty($event->presenterDepartment)) {
        echo htmlentities($event->presenterDepartment);
        if (!empty($event->presenterInstitution)) {
            echo '<br />';
        }
    }

    if (!empty($event->presenterInstitution)) {
        echo $event->presenterInstitution . '<br />';
    }
?>
</p>

<p>
<strong>
    <?php 
    $start_date = strtotime($event->startDate);
    if (date('Y-m-d', time()) == date('Y-m-d', $start_date)) {
        echo "TODAY, ";
    } else {
        echo ($event->isDifferentDay ? '<span class="seminar-event-day-different">' : '' ) . date('l', $start_date) . ($event->isDifferentDay ? '</span>' : '' ) . ', ';
    }
    echo date('F j, Y', $start_date);
    ?>
    </strong><br />
    <?php    
    echo date('g:i a', $start_date);
    if (!empty($event->endDate)) {
        echo '-' . date('g:i a', strtotime($event->endDate));    
    ?><br />
    <?php
    if ($event->isDifferentDay) :
        ?><span class="seminar-event-differentday">(different day)</span><br /><?php
    endif;
    if (!empty($event->location)) :
        echo makeLinks($event->location) . '<br />';
      endif;
      if (!empty($event->onlineLink)) :
        echo 'Online - ';
        if ($event->isOnlineLinkSecured) :
          echo '<a href="' . aae_event_securedonline_link($event->id) . '" target="_blank">Login with UW-Madison NetID to view link</a><br />';
          echo '<em>If you do not have a UW-Madison NetID, you can contact the seminar organizer or consult the <a href="mailto:aae-seminars+subscribe@g-groups.wisc.edu">seminar email announcement</a>.</em>';
        else :
          echo '<a href="' . $event->onlineLink . '" target="_blank">' . $event->onlineLink . '</a>';
        endif;
      endif;
     
    }
?>

    </p>

<p>
    <?php echo $event->abstract;?>
    </p>

<?php
if (count($event->files) > 0) :?>
<p>
    <strong>Download File(s)</strong>
    <ul>
<?php
foreach($event->files as $file):
?>
        <li>
            <a href="<?php echo $file->uri;?>"><?php echo empty($file->linkText) ? $file->filename : $file->linkText;?></a>
        </li>
<?php
endforeach;?>
    </ul>
</p>
<?php
endif;?>
</div> <!-- end of right column -->
</div> <!-- end of row -->
<?php  // revert timezone
  date_default_timezone_set($wp_timezone);
  ?>