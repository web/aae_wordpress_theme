<?php

$people = get_taylorhall();

?>
<table class="stack hover">
    <thead>
        <tr>
            <th>Name</th>
            <th></th>
            <th>Office</th>
            <th>Phone</th>
            <th>Email</th>

</thead>
<tbody>
<?php
foreach($people as $person) :
?>
<tr>
  <td><strong><?php echo $person->lastName . ', ' . $person->firstName;?></strong></td>
  <td><?php echo $person->title ?></td>
  <td><?php
    if (!empty($person->officeAddress)) :
        foreach($person->officeAddress as $address):
            echo $address->addressLine1 . '<br />';
        endforeach;
    endif;
        ?></td>
  <td><?php
    if (!empty($person->officePhone)) :
        foreach($person->officePhone as $phone):
            echo $phone . '<br />';
        endforeach;
    endif;
        ?></td>
  <td><?php echo (!$person->isEmailPrivate) ? hide_email($person->email) : '';?></td>
</tr>
<?php
endforeach;
?>
</tbody>
</table>