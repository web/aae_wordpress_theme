<?php
  

// taken from uw-theme/accordion-panel.php 10/16/2019
if( !wp_script_is('uw-accordion', 'enqueued') ): 
  // Register the script

  wp_register_script('uw-accordion', get_template_directory_uri().'/dist/js/uw-accordion.min.js', null, '1.0.0');
  wp_enqueue_script('uw-accordion');


  //Add the uw accordion init inline script
  $uw_accordion_init = "
    var uw_accordions = jQuery('.uw-accordion');

    uw_accordions.each(function(i){
      var accordion_el = jQuery(this);
      var uw_accordion = new UWAccordion(this);

      var expandButton = accordion_el.find('.uw-accordion-expand-all');
      var collapseButton = accordion_el.find('.uw-accordion-collapse-all');

      // toggle the states of the two buttons and expand or collapse all
      jQuery(this).find('.uw-accordion-controls > button').on('click', function(e){
        var clickedButton = jQuery(this);
        var otherButton = clickedButton.hasClass('uw-accordion-expand-all') ? collapseButton : expandButton;

        clickedButton.prop('disabled', true);
        clickedButton.attr('aria-pressed', true);
        otherButton.prop('disabled', false);
        otherButton.attr('aria-pressed', false);

        if (clickedButton.text() == 'Expand all') {
          uw_accordion.openAll();
        }
        if (clickedButton.text() == 'Collapse all') {
          uw_accordion.closeAll();
        }

      });

      // On Pnael toggle, adjust the expand, collapse button states
      jQuery(this).on('panel-toggle', function(e) {

        // es5-friendly array includes test
        var contains = function(a, obj) {
            for (var i = 0; i < a.length; i++) {
                if (a[i] === obj) {
                    return true;
                }
            }
            return false;
        }

        states = uw_accordion.states.map(function(state) {
          return state.state;
        });

        collapseButton.attr('aria-pressed', !contains(states, 'open'));
        collapseButton.prop('disabled', !contains(states, 'open'));
        expandButton.attr('aria-pressed', !contains(states, 'closed'));
        expandButton.prop('disabled', !contains(states, 'closed'));

      });

      // debounce function
      var uw_debounce = function(func, wait, immediate) {
        var timeout;
        return function() {
          var context = this, args = arguments;
          var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
          };
          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
        };
      };

      // watch for resize of accordion
      window.addEventListener('resize', uw_debounce(function() {
        uw_accordion.calculateAllPanelsHeight();
      }, 50));

    });

  ";

  wp_add_inline_script( 'uw-accordion', $uw_accordion_init );
  endif;

  // Switch timezone temporarily
  $wp_timezone = date_default_timezone_get();
  date_default_timezone_set('America/Chicago');
  
  $events = aae_seminar_termbydate(time());


  if ( $events !== FALSE ):
  
    $current_seminar = -1;

    if (!empty($events)):?>
<div class="uw-accordion">
  <p class="show-for-sr">This is an accordion element with a series of buttons that open and close related content panels.</p>

  <div class="uw-accordion-controls">
    <button class="uw-button-unstyle uw-accordion-expand-all" aria-label="Expand all panels" aria-pressed="false">Expand all</button><button class="uw-button-unstyle uw-accordion-collapse-all" aria-label="Collapse all panels" aria-pressed="true" disabled>Collapse all</button>
  </div>
    <?php
      foreach ($events as $event):
        if ($current_seminar !== $event->seminarId):
          if ($current_seminar !== -1):
          // not the first loop - close the previous <div class="uw-accordion-panel  AND <div class="uw-accordion-panel-inner
          ?></div></div><?php
          endif;  // $current_seminar !== -1

        // now print the seminar header as well as the divs to start the content area?>
    <h2 class="uw-accordion-header">
      <?php echo $event->seminarTitle; ?>
    </h2>        
    <div class="uw-accordion-panel -uw-accordion-is-hidden" role="region">
      <div class="uw-accordion-panel-inner"><?php    
        $current_seminar = $event->seminarId;  // set $current_seminar to this seminar's ID
        endif;  // $current_seminar !== $event->SeminarID

        $start_date = strtotime($event->startDate);
      ?>
      <div class="seminar-event" style="padding-bottom:20px;">
        <div class="seminar-event-date"><?php echo date('l, F j, Y',$start_date); ?></div>
        <div class="seminar-event-title"><a href="/events/view/S<?php echo $event->id ?>"><?php echo $event->title;?></a></div>
        <div class="seminar-event-presenter"><?php echo $event->presenterName;
          if ($event->isJobMarket):
            echo ' (JMC)';
          endif;
          ?>
          <?php echo !empty($event->presenterInstitution) ? ', ' . $event->presenterInstitution : null;?></div>
        <div class="seminar-time-place"><?php echo date('g:ia', $start_date) . ' ' . $event->location;?></div>
        </div>
        <?php
      endforeach;?>
      </div>  <!-- the FINAL closing <div class="uw-accordion-panel-inner -->
    </div>  <!-- the FINAL closing <div class="uw-accordion-panel -->
</div> <!-- end uw-accordion -->
    <?endif; //(!empty($events))?>

<?php
else:
    return FALSE;
endif;   // ( $events !== FALSE )
endif;  // I DON'T KNOW - PHP complained without this one
  // revert timezone
  date_default_timezone_set($wp_timezone);

