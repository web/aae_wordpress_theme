<div class="uw-pe-group_of_links">
<div class="uw-content-box uw-link-list-columns">
<?php

// Group of Links
$header_text = "Seminars";
$seminars = aae_seminars_get();

if ( !empty($header_text) )
  echo '<h4>' . $header_text . '</h4>';


  if( !empty($seminars) ): ?>
    <ul class="uw-link-list">

      <?php foreach($seminars as $seminar) :
       
        ?>

        <li>
          <a href="/events/seminars/<?php echo $seminar->slug; ?>/">
            <?php echo $seminar->title . ' ' . get_svg('uw-symbol-more', array("aria-hidden" => "true")); ?>
          </a>
        </li>
      <?php endforeach; ?>
      <li><a href="/events/seminars/">View All Seminars<?php echo get_svg('uw-symbol-more', array("aria-hidden" => "true")); ?></a>
    </ul>
  <?php endif;

?>
  </div>
</div>