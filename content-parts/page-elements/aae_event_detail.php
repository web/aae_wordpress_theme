<?php
$eventid = get_query_var('eventid');
$event_type_id = strtolower(substr($eventid, 0,1));

switch( $event_type_id ) {
    case 's':
        //seminar
        get_template_part( 'content-parts/page-elements/seminar', 'detail' );
        break;
    case 'd':
        //seminar
        get_template_part( 'content-parts/page-elements/deptevent', 'detail' );
        break;        
}
?>
