<?php
$people_list_title = get_sub_field('listing_title');
$people_list_type = get_sub_field('people_list_type');
$individual_people = get_sub_field('individual_people');
$terms = get_sub_field('faculty_type');
$research_area = get_sub_field('faculty_research_area');

// checks if user has selected specific
// taxonomy terms and set the query accordingly
if( $people_list_type == "Faculty/Staff by category" && $terms ):
	$args = array(
		'staff_type' => $terms,
		'orderby' => 'LastName',
		'order' => 'ASC'
	);

elseif($people_list_type == "Faculty/Staff by research area" && $research_area) :
	$args = array(
		'research_area' => $research_area,
		'orderby' => 'LastName',
		'order' => 'ASC'
	);
elseif($people_list_type == "Select individual people" && $individual_people) :
	$args = array(
		'people' => $individual_people,
		'orderby' => 'LastName',
		'order' => 'ASC'
	);

else :
$args = array(
	'posts_per_page' => -1,
	'post_type' => 'uw_staff',
	'orderby'=> 'title',
	'order' => 'ASC'
);
endif;

?>

<?php
if( get_people($args) ) :
	$columns		= get_sub_field( 'columns');
	$foundation_grid_cols = '';
	$one_column_layout = false;
	$last_to_first		= get_sub_field( 'last_name_first');
	$show_email			= get_sub_field( 'show_email' );
	$show_phone			= get_sub_field( 'show_phone' );
	$show_title			= get_sub_field( 'show_title' );
	$show_linkedin		= get_sub_field( 'show_linkedin' );
	$show_office		= get_sub_field( 'show_office' );
	$show_interests		= get_sub_field( 'show_interests' );
	$show_streetaddress	= get_sub_field( 'show_street_address');
	$show_citystatezip	= get_sub_field( 'show_citystatezip' );

	$show_bio	= get_sub_field( 'show_bio' );
	$display_photos = get_sub_field('display_photos');
	//$display_photos = false;
	$image_size = get_sub_field('image_size');
	$custom_width = get_sub_field('custom_image_width');
	$custom_height= get_sub_field('custom_image_height');

	if ( $columns == 3 ) { $foundation_grid_cols = 4; } // Default
	elseif ( $columns == 2 ) { $foundation_grid_cols = 6; }
	elseif ( $columns == 4 ) { $foundation_grid_cols = 3; }
	else { $foundation_grid_cols = 12; $one_column_layout = true; }

  if ($people_list_title) :
    echo '<h2 class="text-center uw-mini-bar-center">' . $people_list_title . '</h2>';
  endif;
  ?>
  <div class="faculty-list">

  <?php
    $people = get_people($args);

	foreach($people as $person) :

		// if it's by research area, only include Faculty (webgroup==Faculty)
		if ($people_list_type != "Faculty/Staff by research area" || ($people_list_type == "Faculty/Staff by research area" && $person->webGroup == 'Faculty')) :
	// $image_id = get_field( 'headshot', $person->ID );

?>

<div class="faculty-member column small-12 medium-<?php echo $foundation_grid_cols ?>">
	<div class="faculty-member-content">

	<?php
	if ( $one_column_layout ) :
		echo '<div class="row">';
		if ( $display_photos ) :
			echo '<div class="column shrink">';
		endif;
	endif;

	if($display_photos) :
		if ( $image_size == 'Thumbnail (site default)') :
			$image_src = 'thumbnail';
			$image_class = 'thumbnail';
		elseif ($image_size == 'Custom') :
			$image_src = array($custom_width, $custom_height);
			$image_class = 'custom';
		else :
			$image_src = 'uw-headshot';
			$image_class = 'default';
		endif;
	?>
		<div
			class="faculty-image<?php echo ' ' . $image_class; ?>"
			<?php if($image_size == 'Custom') : //set custom dimensions as max-width and max-height ?>
				style="max-width: <?php echo $custom_width; ?>px; max-height: <?php echo $custom_height; ?>px;"
			<?php endif; ?>
			>
			<a href="<?php echo get_person_link($person) ?>" tabindex="-1" aria-hidden="true">
				<?php
				if ( !empty($person->photo) and !$person->isPhotoPrivate ) :
					$img_size = array('width'=>$custom_width,'height'=>$custom_height);
					echo aae_headshot_imgtag($person->photo, $img_size);
				else : ?>
					<img class="buckyhead" src="<?php  echo get_stylesheet_directory_uri() . '/dist/img/no-photo.png'; ?>"/>
				<?php endif; ?>
			</a>
		</div>
	<?php endif; //end if($display_photos) ?>

	<?php
	if ( $one_column_layout ) :
		if($display_photos) :
			echo '</div>';
		endif;
		echo '<div class="column">';
	endif;
	?>

		<h3><a href="<?php echo get_person_link($person); ?>">
			<?php echo $last_to_first ? $person->lastName. ', ' . $person->firstName : $person->firstName . ' ' . $person->lastName ?>
		</a></h3>
		<?php echo ($show_title and !empty($person->title)) ? '<p>' . $person->title . '</p>' : ''; ?>

		<?php echo !empty($person->onLeave) ? '<p class="on-leave">' . $person->onLeave . '</p>' : '' ?>

		<?php echo ($show_email and !$person->isEmailPrivate) ? '<p><a href="mailto:' . $person->email . '">' . $person->email . '</a></p>' : '' ?>

		<?php echo ($show_phone and !empty($person->officePhone)) ? '<p>' . $person->officePhone[0] . '</p>' : '' ?>

		<?php if (($show_office or $show_streetaddress or $show_citystatezip) and $person->officeAddress) :
					foreach($person->officeAddress as $address) :
						echo $show_office ? $address->addressLine1 . '<br />' : '';
						echo $show_streetaddress ? $address->addressLine2 . '<br />' : '';
						echo $show_citystatezip ? $address->city . ', ' . $address->state  . ' ' . $address->zip . '<br />' : '';
					endforeach;
				else :
					echo '';
				endif;

		//$linkedin_icon = get_field( 'linkedin', $person->ID );

		if ($show_interests and $person->interests) :
			echo '<p class="interests">' . $person->interests . '</p>';
		endif;

		if ( $show_linkedin ) : ?>
			<?php if ( !empty( $linkedin_icon ) ) : ?>
				<ul class="uw-social-icons">
					<li class="uw-social-icon">
						<a href="<?php echo $linkedin_icon; ?>"> <?php echo get_svg('uw-symbol-linkedin', array("aria-hidden" => "true")) ?>
						</a>
					</li>
				</ul>
			<?php endif; ?>
		<?php endif;?>

		<?php echo $show_bio ? '<p class="bio">' . $person->narrative  . '</p>' : '' ;

	if ($one_column_layout ) :
		echo '</div></div>';
	endif;
	?>
	</div>
</div>

<?php
// endif if for (listing = ResearchArea && webgroup=faculty)
endif;?>

<?php endforeach; wp_reset_postdata(); endif;  ?>
</div>
