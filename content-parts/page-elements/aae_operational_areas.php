<?php

$op_areas = get_oparea_listing();
$custom_width = 240;
$custom_height = 240;
if (count($op_areas) > 0) :
?>

<?php
    foreach($op_areas as $op_area) :

?>
<div class="aae-oparea">
<h2><?php echo $op_area->description;?></h2>
<div class="faculty-list">
<?php
        foreach($op_area->people as $person) : 
?>
    <div class="faculty-member column small-12 medium-4">
	<div class="faculty-member-content"><?php
			$image_src = array($custom_width, $custom_height);
			$image_class = 'custom';
?>
            <div
			class="faculty-image<?php echo ' ' . $image_class; ?>"
			    style="max-width: <?php echo $custom_width; ?>px; max-height: <?php echo $custom_height; ?>px;">
			<a href="<?php echo get_person_link($person) ?>" tabindex="-1" aria-hidden="true">
				<?php
			$photo = $person->photo;
			if ( $photo and !$person->isPhotoPrivate ) :
			echo '<p>' . aae_headshot_imgtag($photo, 'medium', true) . '</p>';
			else :
			echo '<p><img src="' . get_stylesheet_directory_uri() . '/dist/img/no-photo.png"/></p>';
			endif;				?>
			</a>
        </div>  <!--/faculty-image-->         
       	<h3><a href="<?php echo get_person_link($person); ?>">
		<?php echo  $person->firstName . ' ' . $person->lastName; ?>
        </a></h3>       
		<?php echo !empty($person->responsibilities) ? '<p>' . $person->responsibilities . '</p>' : ''; ?>

		<?php echo !$person->isEmailPrivate ? '<p>' . $person->email . '</p>' : '' ?>

		<?php echo !empty($person->officePhone) ? '<p>' . $person->officePhone . '</p>' : '' ?>

		<?php if (!empty($person->officeAddress)) :
					echo $person->officeAddress . '<br />';
				else :
					echo '';
				endif;       ?>

    </div><!-- /faculty-member-content-->
    </div><!--/faculty-member--><?php
        endforeach;
?></div> <!-- /faculty-list-->
</div><!-- /aae-oparea-->
<!--/oparea-listing--><?php        
    endforeach;
endif;
?>
