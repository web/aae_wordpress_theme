<?php
  // Switch timezone temporarily
  $wp_timezone = date_default_timezone_get();
  date_default_timezone_set('America/Chicago');


$eventid = substr(get_query_var('eventid'),1);

$event = aae_dept_events_get($eventid);



?>

<h2><?php echo $event->Summary ?></h2>

<p>
<em><?php echo $event->Category . ' - ' , $event->SubCategory;?></em>
</p>

<p>
<strong>
    <?php 
    $start_date = strtotime($event->StartDate);
    if (date('Y-m-d', time()) == date('Y-m-d', $start_date)) {
        echo "TODAY, ";
    } else {
        echo date('l', $start_date) . ', ';
    }
    echo date('F j, Y', $start_date);
    ?>
    </strong><br />
    <?php echo $event->Location;?><br />
    <?php echo date('g:i a', $start_date);
    if (!empty($event->EndDate)) {
        echo '-' . date('g:i a', strtotime($event->EndDate));
    }
?>

    </p>


    <?php echo $event->DescriptionHTML;?>



<?php  // revert timezone
  date_default_timezone_set($wp_timezone);
  ?>