
<?php

//hard code these
// $columns		= get_sub_field( 'columns');
// $foundation_grid_cols = '';
// $one_column_layout = false;
// $last_to_first		= get_sub_field( 'last_name_first');
// $show_email			= get_sub_field( 'show_email' );
// $show_phone			= get_sub_field( 'show_phone' );
// $show_title			= get_sub_field( 'show_title' );
// $show_linkedin		= get_sub_field( 'show_linkedin' );
// $show_office		= get_sub_field( 'show_office' );
// $show_interests		= get_sub_field( 'show_interests' );
// $show_streetaddress	= get_sub_field( 'show_streetaddress' );
// $show_citystatezip	= get_sub_field( 'show_citystatezip' );

$columns		= 1;
$foundation_grid_cols = '';
$one_column_layout = false;
$last_to_first		= false;
$show_email			= true;
$show_phone			= false;
$show_title			= false;
$show_linkedin		= false;
$show_office		= false;
$show_interests		= true;
$show_streetaddress	= false;
$show_citystatezip	= false;

$show_bio	= false;
$display_photos = true;
//$display_photos = false;
$image_size = 'Custom';
$custom_width = '240';
$custom_height= '240';

if ( $columns == 3 ) { $foundation_grid_cols = 4; } // Default
elseif ( $columns == 2 ) { $foundation_grid_cols = 6; }
elseif ( $columns == 4 ) { $foundation_grid_cols = 3; }
else { $foundation_grid_cols = 12; $one_column_layout = true; }

	$jmcGroups = get_jmc_groups();

	foreach($jmcGroups as $jmcGroup) :
		
	// // only doing PhDs now
	// $degree = new \stdClass;
	// $degree->id = 1;
	// $degree->description = 'Ph.D. Candidate'
  ?>


  <?php
    $people = get_jobmarket_candidates($jmcGroup->id);

	if (count($people) > 0) :

		if ($jmcGroup->description) :
			echo '<h2 class="text-center uw-mini-bar-center">' . $jmcGroup->description . '</h2>';
		endif;?>

<div class="faculty-list">
<?php
	foreach($people as $person) :
	// $image_id = get_field( 'headshot', $person->ID );

?>


<div class="faculty-member column small-12 medium-<?php echo $foundation_grid_cols ?>">
	<div class="faculty-member-content">

	<?php
	if ( $one_column_layout ) :
		echo '<div class="row">';
		if ( $display_photos ) :
			echo '<div class="column shrink">';
		endif;
	endif;

	if($display_photos) :
		if ( $image_size == 'Thumbnail (site default)') :
			$image_src = 'thumbnail';
			$image_class = 'thumbnail';
		elseif ($image_size == 'Custom') :
			$image_src = array($custom_width, $custom_height);
			$image_class = 'custom';
		else :
			$image_src = 'uw-headshot';
			$image_class = 'default';
		endif;
	?>
		<div
			class="faculty-image<?php echo ' ' . $image_class; ?>"
			<?php if($image_size == 'Custom') : //set custom dimensions as max-width and max-height ?>
				style="max-width: <?php echo $custom_width; ?>px; max-height: <?php echo $custom_height; ?>px;"
			<?php endif; ?>
			>
			<a href="<?php echo get_jobmarketcandidate_link($person) ?>" tabindex="-1" aria-hidden="true">
				<?php
				if ( !empty($person->photo) && !$person->isPhotoPrivate) :
					$img_size = array('width'=>$custom_width,'height'=>$custom_height);
					echo aae_headshot_imgtag($person->photo, $img_size);
				else : ?>
					<img class="buckyhead" src="<?php  echo get_stylesheet_directory_uri() . '/dist/img/no-photo.png'; ?>"/>
				<?php endif; ?>
			</a>
		</div>
	<?php endif; //end if($display_photos) ?>

	<?php
	if ( $one_column_layout ) :
		if($display_photos) :
			echo '</div>';
		endif;
		echo '<div class="column">';
	endif;
	?>

		<h3><a href="<?php echo get_jobmarketcandidate_link($person); ?>">
		<?php echo $last_to_first ? $person->lastName. ', ' . $person->firstName : $person->firstName . ' ' . $person->lastName ?>
		</a></h3>
		<?php echo ($show_email && !$person->isEmailPrivate) ? '<p><a href="mailto:' . $person->email . '">' . $person->email . '</a></p>' : '';

		if ($show_interests && $person->fields) :
			echo '<p class="interests">' . $person->fields . '</p>';
		endif;
		
		if ($person->jobMarketPaperTitle) :
			echo '<p class="jmc-paper"><strong>Job Market Paper:</strong> <br />';
			if ($person->jobMarketPaperLink) :
				echo '<a href="' . $person->jobMarketPaperLink . '">';
			endif;
			echo '<span class="jmc-paper-title">"'. $person->jobMarketPaperTitle . '"</span></p>';
			if ($person->jobMarketPaperLink) :
				echo '</a>';
			endif;
		endif;

		if ($person->advisors) :
			echo '<p class="advisors"><strong>References:</strong> <br /><span class="advisors">'. nl2br($person->advisors) . '</span></p>';
		endif;

		if ( $show_linkedin ) : ?>
			<?php if ( !empty( $linkedin_icon ) ) : ?>
				<ul class="uw-social-icons">
					<li class="uw-social-icon">
						<a href="<?php echo $linkedin_icon; ?>"> <?php echo get_svg('uw-symbol-linkedin', array("aria-hidden" => "true")) ?>
						</a>
					</li>
				</ul>
			<?php endif; ?>
		<?php endif;?>

		<?php 
	if ($one_column_layout ) :
		echo '</div></div>';
	endif;
	?>
	</div>
</div>

<?php endforeach; wp_reset_postdata(); 

	
endif;    //test if any people were returned for a particular degree

echo '</div>';
	
endforeach;   //jmc groups for loop
  ?>
</div>
