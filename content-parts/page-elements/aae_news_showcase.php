<?php

$columns		= 1;
$foundation_grid_cols = '';
$one_column_layout = false;
$list_title = get_sub_field('listing_title');
$show_featured_image = get_sub_field('show_featured_image');
$number_of_articles = get_sub_field('number_of_articles');
$news_categories = get_sub_field('news_categories');
$new_categories_ids = array_column($news_categories, 'term_id');
$news_tags = get_sub_field('news_tags');
$post_list = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'category__in' => $new_categories_ids, 'posts_per_page'=>$number_of_articles)); 
$total_returned_posts = $post_list->found_posts;

if ( $columns == 3 ) { $foundation_grid_cols = 4; } // Default
elseif ( $columns == 2 ) { $foundation_grid_cols = 6; }
else { $foundation_grid_cols = 12; $one_column_layout = true; }
?>
<div class="uw-news-showcase">
<?php
if ($list_title) :
    echo '<h2 class="text-center uw-mini-bar-center">' . $list_title . '</h2>';
  endif;    

if ( $post_list->have_posts() ) : 
$item_counter = 0;
?>

<?php
while ( $post_list->have_posts() ) :

$print_row = $item_counter == 0 || $item_counter % 3 == 0;
$print_end_row = (
    ($item_counter + 1) % 3 == 0 || 
    $item_counter+1 == $number_of_articles ||
    $item_counter+1 == $total_returned_posts);
    
if ($print_row ) :?>
<div class="uw-row top-stories">
<?php endif; // $print_row
     
        $post_list->the_post();

        
        $news_url = get_field('news_url');
        $link_directly = get_theme_mod('uwmadison_post_external_links', true);
        if($link_directly && !empty($news_url)) {
            $permalink = $news_url;
        } else {
            $permalink = get_permalink();
        }
        
        ?>
    <div class="uw-col ruled post-<?php get_the_ID()?> post format-standard">
        <?php
        if ($show_featured_image) :
        
            $image = get_stylesheet_directory_uri() . "/dist/img/trans-featured-img.png";
            $image_class = "";
            if (has_post_thumbnail()) :
                $image = get_the_post_thumbnail_url( get_the_ID());
                $image_class = "uw-img-border";
            endif;
            
        ?>
        <a class="featured-image" href="<?php echo esc_url( $permalink ); ?>"><img src="<?php echo $image; ?>" class="<?php echo $image_class; ?>" /></a>
        <?php
        endif;?>
        <h3 class="headline"><a href="<?php echo esc_url( $permalink ); ?>"><?php the_title(); ?></a></h3>
        <span class="meta"><span class="date"><?php echo get_the_date('F j, Y')?></span></span>
        <p class="lead"><?php the_excerpt();?></p>
    </div>   
    <?php
    if ($print_end_row) :?>       
</div> <!-- /uw-row top-stories -->
<?php endif; // $print_row

        $item_counter++;
        endwhile; 

wp_reset_postdata(); ?>
  <?php else : ?>
    <p><?php _e( 'There no posts to display.' ); ?></p>
  <?php endif; ?>

  <div class="uw-news-showcase-footer">
    <?php


    // unabashedly stolen from get_the_category_list()
    // https://developer.wordpress.org/reference/functions/get_the_category_list/

    $parents = '';
    $rel = ( is_object( $wp_rewrite ) && $wp_rewrite->using_permalinks() ) ? 'rel="category tag"' : 'rel="category"';
    $thelist = '';
    $separator = ', ';
    $i = 0;
    foreach ( $news_categories as $category ) {
        if ( 0 < $i ) {
            $thelist .= $separator;
        }
        switch ( strtolower( $parents ) ) {
            case 'multiple':
                if ( $category->parent ) {
                    $thelist .= get_category_parents( $category->parent, true, $separator );
                }
                $thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name . '</a>';
                break;
            case 'single':
                $thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>';
                if ( $category->parent ) {
                    $thelist .= get_category_parents( $category->parent, false, $separator );
                }
                $thelist .= "$category->name</a>";
                break;
            case '':
            default:
                $thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name . '</a>';
        }
        ++$i;
    }

    // taken from uwmadison_entry_meta()
    printf( '<span class="cat-links">' . esc_html__( 'View more in %1$s', 'uw-theme' ) . '</span>', $thelist ); // WPCS: XSS OK.

  ?>
  </div> <!-- /uw-news-showcase-footer-->
  </div>  <!-- /uw-news-showcase-->


<?php
if (true==false):
    ?>
<h2 class="text-center uw-mini-bar-center"><?php echo $list_title?></h2>
<div class="column small-12 medium-<?php echo $foundation_grid_cols ?>">
    <div class="news-showcase-content">
    <?php
	if ( $one_column_layout ) :
		echo '<div class="row">';
		if ( $show_featured_image ) :
			echo '<div class="column shrink">';
		endif;
	endif;

    if($show_featured_image) :
	?>

	<?php endif; //end if($show_featured_image) ?>
                    <?php var_dump($post_list)?>
	<?php
	if ( $one_column_layout ) :
		if($show_featured_image) :
			echo '</div>';
		endif;
		echo '<div class="column">';
	endif;
	?>    
    </div>
</div>
<?php endif; // TEMP true==false
?>

