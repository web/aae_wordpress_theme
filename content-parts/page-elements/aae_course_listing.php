    <?php
//get a term for 2 years back (possibly 6 total terms)    
$term = get_uw_term(strtotime("-2 year", time()));
$max_show_syllabus = 2;
$course_printed_syllabus = 0;   //how many syllabi have been printed for a particular course
$courses = get_courses();
$syllabi = get_syllabi_to_term($term->termCode);
$first_loop = true;
$hundreds = '';
$current_syllabus_ptr = 0;

foreach($courses as $course) : 
    if (!$first_loop and $hundreds != substr(strval($course->courseNumber),0,1)) :
    echo '<hr />';
    endif;
    $hundreds = substr(strval($course->courseNumber),0,1);
?>

    <div class="aae-course" id="aae<?php echo $course->courseNumber?>"><a name="aae<?php echo $course->courseNumber?>"></a>
    <span class="aae-course-title"><?php echo $course->courseNumberAndTitle?>.</span>
    <?php 
        echo !empty($course->crosslistings) ? ' (' . $course->crosslistings .') ' : '';
        if (!empty($course->offerings)) :
            $str_offer = '';
            foreach($course->offerings as $offering):
                if (!empty($str_offer)) $str_offer .= ', ';
                $str_offer .= strtolower($offering->description);
            endforeach;
            echo 'offered ' . $str_offer . '; ';
        endif;
        echo !empty($course->credits) ? $course->credits . ' credits ' : '';
        if (!empty($course->breadth) or !empty($course->level)) :
        echo '(';
        echo !empty($course->breadth) ? $course->breadth . '-' : '';
        echo !empty($course->level) ? $course->level : '';
        echo ')';
        endif;
        echo ' ' . $course->description;
        echo !empty($course->preRequisites) ? ' Pre-Reqs: ' . $course->preRequisites : '' ;

        //web page and syllabi list
        $syllabi_web_list = '';

        //loop through syllabi collection
        $course_printed_syllabus = 0;

        while ($current_syllabus_ptr < count($syllabi) && $syllabi[$current_syllabus_ptr]->courseNumber <= $course->courseNumber && $syllabi[$current_syllabus_ptr]->courseID == $course->id):
            $current_syllabus = $syllabi[$current_syllabus_ptr];

            if ($current_syllabus->courseNumber = $course->courseNumber && $course_printed_syllabus < $max_show_syllabus) :
                $syllabi_web_list .= '<li class="syllabus"><a href="'. $current_syllabus->uri . '" ';
                $syllabi_web_list .= ' title="' . $course->courseNumberAndTitle . ' ' . $current_syllabus->term . ' Syllabus">';
                $syllabi_web_list .= 'Syllabus ' .$current_syllabus->term;
                $syllabi_web_list .= '</a> (' . $current_syllabus->instructor . ')</li>';
                $course_printed_syllabus++;
            endif;
            $current_syllabus_ptr++;
        endwhile;
        if (!empty($syllabi_web_list)):
            echo '<ul class="courses-syllabi-web">' . $syllabi_web_list . '</ul>';
        endif;

        ?>
    </div><?php
    $first_loop = false;
endforeach;

$js_script = 'jQuery(document).ready(function(){});';

add_action('wp_head', 'courselist_custom_styles', 100);
function courselist_custom_styles() {
    echo '<style>:target {background:yellow;}</style>';
}
    ?>
