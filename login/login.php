<?php

//customize login form
function login_function() {
    add_filter( 'gettext', 'username_change', 20, 3 );
    function username_change( $translated_text, $text, $domain ) 
    {
        if ($text === 'Username or Email Address') 
        {
            $translated_text = 'SKIP - CLICK "LOGIN WITH CAMPUS NETID" BELOW';
        } elseif ($text === 'Password') {
            $translated_text = 'SKIP - CLICK "LOGIN WITH CAMPUS NETID" BELOW';
        }
        return $translated_text;
    }
}
add_action( 'login_head', 'login_function' );

function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/login/style.css' );
    //wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/style-login.js' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

?>