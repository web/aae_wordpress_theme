
<?php
/**
 * The template to display a single post.
 *
 * Template Name: Scholar Detail
 */

 /**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */


$fullname = get_query_var('fullname');
$scholar_listing_term = get_uw_term_bycode(get_query_var('uwterm'));
$scholar = scholar_detail($scholar_listing_term, $fullname);

//if a user object wasn't returned, this graudate doesn't exist - 404
if (empty($scholar)) {
  force_404();
}

function scholar_custom_title($title_parts) {
    $fullname = get_query_var('fullname');
    $scholar_listing_term = get_uw_term_bycode(get_query_var('uwterm'));
    $scholar = scholar_detail($scholar_listing_term, $fullname);
     $title_parts['title'] = $scholar->firstName . ' ' . $scholar->lastName . ' | ' . $scholar_listing_term->shortDescription . ' scholars';

    return $title_parts;
}
add_filter( 'document_title_parts', 'scholar_custom_title' );

// title of the scholar's breadcrumb
function scholar_custom_breadcrumb( $title, $id = null ) {
  if ($id == scholar_detail_pageid()) :
    $fullname = get_query_var('fullname');
    $scholar_listing_term = get_uw_term_bycode(get_query_var('uwterm'));
    $scholar = scholar_detail($scholar_listing_term, $fullname);
    return $scholar->firstName . ' ' . $scholar->lastName;
  endif;

  return $title;
}
add_filter( 'the_title', 'scholar_custom_breadcrumb', 10, 2 );

// title of scholars list breadcrumb
function scholarlist_custom_breadcrumb( $title, $id = null ) {
	if ($id == scholar_list_pageid()) {
		return scholarlist_custom_breadcrumb_actual($title, $id);
	}

	return $title;
}
add_filter( 'the_title', 'scholarlist_custom_breadcrumb', 10, 2 );

get_header(); ?>

<div id="page" class="content page-builder">
	<main id="main" class="site-main">

	<?php if ( site_uses_breadcrumbs() ) { custom_breadcrumbs(); } ?>

  <article class=" page type-page hentry">
<?php
  $firstName = $scholar->firstName;
  $lastName = $scholar->lastName;
  $middleName = $scholar->middleName;


?>

	<div class="entry-content">
    <div class="uw-outer-row row-1 has_text_block default-background">
      <div class="uw-inner-row">
        <div class="uw-column one-column">
<div class="notsure">
              <h1 class="page-title uw-mini-bar"><?php echo $firstName. ' ' .$middleName. ' ' .$lastName ?></h1>

      <dl class="faculty-extra"><?php
      // Year in school
      if (!empty($scholar->career)) :?>
        <dt class="faculty-extra-label">Year</dt>
        <dd class="faculty-extra-value"><?php echo $scholar->career; ?></dd>
        <?php
        endif;
        
      // degree
      if (!empty($scholar->degree)) :?>
        <dt class="faculty-extra-label">Degree</dt>
        <dd class="faculty-extra-value"><?php echo $scholar->degree ?></dd>
      <?php
      endif;

      // PhD Minor
      if (!empty($scholar->phdMinor)) :?>
        <dt class="faculty-extra-label">Ph.D. Minor</dt>
        <dd class="faculty-extra-value"><?php echo $scholar->phdMinor ?></dd>
        <?php
      endif;

      // PhD Dissertation title
      if (!empty($scholar->phdDissertation)) :?>
        <dt class="faculty-extra-label">Dissertation Title</dt>
        <dd class="faculty-extra-value"><?php echo $scholar->phdDissertation ?></dd>
        <?php
      endif;

      // PhD Advisor
      if (!empty($scholar->phdAdvisor)) :?>
        <dt class="faculty-extra-label">Advisor</dt>
        <dd class="faculty-extra-value"><?php echo $scholar->phdAdvisor ?></dd>
        <?php
      endif;      

      // Additional degree or major
      if (!empty($scholar->degreeSecond)) :?>
        <dt class="faculty-extra-label">Additional Degree/Major</dt>
        <dd class="faculty-extra-value"><?php echo $scholar->degreeSecond ?></dd>
        <?php
      endif;

      // Certificates
      if (!empty($scholar->certificateOne) || !empty($scholar->certificateTwo)) :?>
        <dt class="faculty-extra-label">Certificate<?php echo !empty($scholar->certificateTwo) ? 's' : null;?></dt>
        <dd class="faculty-extra-value"><?php echo $scholar->certificateOne ?></dd>
        <dd class="faculty-extra-value"><?php echo $scholar->certificateTwo ?></dd>
        <?php
      endif;

      // Hometown
      if (!empty($scholar->hometownCity) and !empty($scholar->hometownState)) :?>
        <dt class="faculty-extra-label">Hometown</dt>
        <dd class="faculty-extra-value"><?php echo $scholar->hometownCity . ', ' . $scholar->hometownState; ?></dd>
        <?php
        endif;

      // Scholarships/Grad Support
      if (!empty($scholar->uwScholarships) || !empty($scholar->otherScholarships) || !empty($scholar->uwGradSupport) 
            || !empty($scholar->bsScholarships) || !empty($scholar->phdAwards)) :?>
        <dl class="faculty-extra">
        <dt class="faculty-extra-label">Scholarships</dt>
        <dd class="faculty-extra-value"><?php
            echo $scholar->bsScholarships;
            echo !empty($scholar->bsScholarships) ? '<br />' : '';
            echo $scholar->phdAwards;
            echo !empty($scholar->phdAwards) ? '<br />' : '';            
            echo str_replace("\r\n", "<br />", $scholar->uwScholarships);
            echo !empty($scholar->uwScholarships) ? '<br />' : '';
            echo str_replace("\r\n", "<br />", $scholar->otherScholarships);
            echo !empty($scholar->otherScholarships) ? '<br />' : '';
            echo str_replace("\r\n", "<br />", $scholar->uwGradSupport) ;
            ?></dd></dl>
        <?php
      endif;

      // Message
      if (!empty($scholar->message)) :?>
        <dt class="faculty-extra-label"><?php
        switch ($scholar->levelId) {
          case 1:
            echo "Future Plans";
            break;
          case 2:
            echo "Statement";
            break;
          }
            ?></dt>
        <dd class="faculty-extra-value">      <?php echo str_replace("\r\n", '<br />', $scholar->message); ?>
        </dd>
        <?php
        endif;
        ?>

        
</div>
  </div></div></div><!-- end of uw-outerrow, inner-row, one-column-->
	</div> <!-- end of entry container -->


  </article>
	</main>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>