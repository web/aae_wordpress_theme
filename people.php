
<?php
/**
 * The template to display a single post.
 *
 * Template Name: People
 */

 /**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
 

$username = get_query_var('netid');
$person = get_person($username);

//if a user object wasn't returned, this person shouldn't have a page
if (empty($person)) {
  force_404();
}

$person_web_group = get_webgroup_for_person($username);

//if they aren't in a web group at all, give a 404 error
if (empty($person_web_group)) {
  force_404();
}

//make sure this user's webgroup matches this page's web group  (staff for staff page, student for student page, etc)
global $post;
$page_web_group_id = null;
switch ($post->ID) {
  case faculty_details_pageid():
    $page_web_group_id = WebGroup::Faculty;
    break;
  case staff_details_pageid():
    $page_web_group_id = WebGroup::Staff;
    break;
  case gradstudent_details_pageid():
    $page_web_group_id = WebGroup::GradStudents;
    break;
  case emeritus_details_pageid():
    $page_web_group_id = WebGroup::Emeriti;
    break;        
  case otherperson_details_pageid():
      $page_web_group_id = WebGroup::Other;
      break;           
}

//if the discovered page_web_group_id doensn't match the user's web group id, then we force a 404
if ($page_web_group_id != $person_web_group->id) {
  force_404();
}

//ok - at this point, the person is legit and able to be shown on the web.  They have a valid web group and it matches this page's web group.
//we can now start to display various parts of the page, based on their classification (web group)

//grad students have their own special wip
$wip_detail = null;
if ($page_web_group_id == WebGroup::GradStudents || $page_web_group_id == WebGroup::Other) {
  $wip_detail = get_gradwip_detail($username);
} else {
  $wip_detail = get_wip_detail($username);
}

// if someone doesn't have a wip, create an empty object so you don't have to constantly check if it's an object
if (!is_object($wip_detail)) :
  $wip_detail = new \stdClass;
endif;


function people_custom_title($title_parts) {
      $username = get_query_var('netid');
      $person = get_person($username);
     $title_parts['title'] = $person->firstName . ' ' . $person->lastName;
    return $title_parts;
}
add_filter( 'document_title_parts', 'people_custom_title' );

function people_custom_breadcrumb( $title, $id = null ) {


    if (people_is_details_page($id)) {
      $username = get_query_var('netid');
      $person = get_person($username);
      return $person->firstName . ' ' . $person->lastName;
    }

    return $title;
}

// converts a youtube link to embed code
function convertYoutube($string) {
  return preg_replace(
      "/[a-zA-Z\/\/:\.]*youtu(?:be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)(?:[&?\/]t=)?(\d*)(?:[a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
      "https://www.youtube.com/embed/$1?start=$2",
      $string
  );
}

add_filter( 'the_title', 'people_custom_breadcrumb', 10, 2 );

get_header(); ?>
<?php if ( site_uses_breadcrumbs() ) { custom_breadcrumbs(); } ?>

<div id="page" class="content">
	<main id="main" class="site-main">



  <article class=" page type-page hentry">
<?php
  $first_name = $person->firstName;
  $last_name = $person->lastName;
  $email = $person->email;

  $title = null;
  $interests = $person->interests;
  $interests_descr = null;
  if ($page_web_group_id == WebGroup::GradStudents) {
    $interests_descr = 'Fields';
    if (property_exists($person, 'degree') && $wip_detail->degree != '') :
      $title = $wip_detail->degree;
    else :
      $title = $person->title;
    endif;
  } else {
    $interests_descr = 'Interests';
    $title = $person->title;
  }
  
?>

	<div class="entry-content">
  <div class="faculty-headshot-contact">
  <div class="faculty-contact">
    <h1 class="page-title uw-mini-bar"><?php echo $first_name. ' ' .$last_name ?></h1>
    <?php echo !empty($title) ? '<h2><p>' . $title . '</p></h2>' : null; ?>
    <?php echo !empty($person->onLeave) ? '<p class="on-leave">' . $person->onLeave . '</p>' : null; ?>
    <?php
    echo !empty($email) ? '<p><a href="mailto:' . $email . '">' . $email . '</a></p>' : null;
    foreach($person->officePhone as $phone) :
      echo '<p>' . $phone . '</p>';
    endforeach;
    foreach($person->officeAddress as $office_address) :
      echo '<p class="faculty-address">';
      echo !empty($office_address->addressLine1) ? $office_address->addressLine1 . '<br />' : null;    
      echo !empty($office_address->addressLine2) ? $office_address->addressLine2 . '<br />' : null; 
      echo (!empty($office_address->city) and !empty($office_address->state) and !empty($office_address->zip)) ? $office_address->city . ', ' . $office_address->state . ' ' . $office_address->zip : null;
      echo '</p>';
    endforeach;

    if (!empty($interests) 
        or (property_exists($wip_detail, 'officeHours') and !empty($wip_detail->officeHours))
        or (property_exists($wip_detail, 'jobMarketPaperTitle') and !empty($wip_detail->jobMarketPaperTitle)) 
      ) :
      ?><dl class="faculty-extra"><?php

      echo !empty($interests) ? '<dt class="faculty-extra-label">' . $interests_descr . '</dt><dd class="faculty-extra-value">' . $interests . '</dd>' : null;

      echo (property_exists($wip_detail, 'officeHours') && !empty($wip_detail->officeHours)) ? '<dt class="faculty-extra-label">Office Hours</dt><dd class="faculty-extra-value">' . $wip_detail->officeHours . '</dd>' : null;

      if (property_exists($wip_detail, 'jobMarketPaperTitle') and !empty($wip_detail->jobMarketPaperTitle)) :
        echo '<dt class="faculty-extra-label">Job Market Paper Title</dt><dd class="faculty-extra-value">"';
        if (!empty($wip_detail->jobMarketPaperLink)) :
          echo '<a href="' . $wip_detail->jobMarketPaperLink . '">';
        endif;
         echo  $wip_detail->jobMarketPaperTitle ;
         if (!empty($wip_detail->jobMarketPaperLink)) :
            echo '</a>';
        endif;
         echo '"</dd>';
      endif;
      ?></dl><?php
    endif;
    //echo !empty($address) ? '<p class="faculty-address">' . $address . '</p>' : null;
    //echo !empty($linkedin) ? '<ul class="uw-social-icons"><li class="uw-social-icon"><a href="' . $linkedin . '">' . get_svg('uw-symbol-linkedin', array("aria-hidden" => "true")) . '</a></li></ul>' : null;
  ?>

  </div>  <!-- end faculty contact-->
  <div class="faculty-headshot">
    <?php
    $photo = $person->photo;
    if ( $photo and !$person->isPhotoPrivate ) :
      echo aae_headshot_imgtag($photo, 'medium', true, 'Headshot of '. $first_name. ' ' .$last_name);
    else :
      echo '<img src="' . get_stylesheet_directory_uri() . '/dist/img/no-photo.png" alt="No headshot available for ' . $first_name . ' ' . $last_name . '"/>';
    endif;
    ?>
  </div> <!-- end faculty-headshot-->
</div>

<?php
if (property_exists($wip_detail, 'narrative')):?>
<div class="faculty-bio">
  <?php echo $wip_detail->narrative; ?>
</div>
<?php
endif;
if (    (property_exists($wip_detail, 'links') and count($wip_detail->links) > 0) 
        or !empty($wip_detail->personalWebsite) 
        or !empty($wip_detail->hasCV) 
        or (property_exists($wip_detail, 'googleScholar') && !empty($wip_detail->googleScholar))
      ) :

?>

<div class="faculty-info-container">
  <!-- start of left column -->
  <div class="large-6">
  <dl class="faculty-extra faculty-info medium-6">
    <dt class="faculty-extra-label">Links</dt>
        <?php if (!empty($wip_detail->personalWebsite)) : ?>
          <dd class="faculty-extra-value">
            <a href="<?php echo $wip_detail->personalWebsite;?>">Personal website</a>
          </dd>
        <?php endif;
          if (!empty($wip_detail->hasCV) && $wip_detail->hasCV == true) : ?>
                  <dd class="faculty-extra-value">
            <a href="<?php echo $wip_detail->cvUri ?>">Curriculum Vitae</a>
          </dd>
        <?php endif;
          if ((property_exists($wip_detail, 'googleScholar') && !empty($wip_detail->googleScholar))) : ?>
                  <dd class="faculty-extra-value">
            <a href="<?php echo$wip_detail->googleScholar ?>">Google Scholar</a>
          </dd>
        <?php endif;        
          if (property_exists($wip_detail, 'links') && count($wip_detail->links) > 0) :
            foreach($wip_detail->links as $link) :?>
          <dd class="faculty-extra-value">
            <?php echo !empty($link->extra) ? $link->extra .', ' : null; ?><a href="<?php echo $link->link;?>"><?php echo $link->linkText; ?></a>
          </dd>
        <?php
            endforeach;
          endif;
        ?>
    </dt>
  </dl>


  <?php
  endif;

if (property_exists($wip_detail, 'affiliations') and count($wip_detail->affiliations) > 0) :?>
    <dl class="faculty-extra faculty-info">
      <dt class="faculty-extra-label">Affiliations</dt>
      <?php
    foreach($wip_detail->affiliations as $affiliation):?>
      <dd class="faculty-extra-value">
      <?php echo $affiliation->title . ', ';
        if (!empty($affiliation->link)) :
          echo '<a href="' . $affiliation->link . '" target="_blank">';
        endif;
        echo $affiliation->organization;
        if (!empty($affiliation->link)) :
          echo '</a>';
        endif;
      ?>
    </dd>
  <?php
    endforeach;
  ?>
    </dt>
  </dl><?php

  endif;

  if (property_exists($wip_detail, 'advisors') and !empty($wip_detail->advisors)) :
  
  ?>
  
  <dl class="faculty-extra faculty-info">
    <dt class="faculty-extra-label">References</dt>
        <?php
          $advisors_arr = explode("\n", $wip_detail->advisors);
          foreach($advisors_arr as $advisor) :?>
          <dd class="faculty-extra-value">
            <?php echo $advisor; ?>
          </dd>
        <?php
          endforeach;
        ?>
    </dt>
  </dl><?php
  endif;

  if (property_exists($wip_detail, 'courses') and count($wip_detail->courses) > 0) :?>
  <dl class="faculty-extra faculty-info">
    <dt class="faculty-extra-label">Courses</dt>
        <?php
          foreach($wip_detail->courses as $course) :?>
          <dd class="faculty-extra-value">
            <?php echo 'AAE ' . $course->courseNumber .': ' . $course->title; ?>
          </dd>
        <?php
          endforeach;
        ?>
    </dt>
  </dl><?php
  endif;
  if (!empty($wip_detail->education)) :?>
  <dl class="faculty-extra faculty-info">
    <dt class="faculty-extra-label">Education</dt>
          <dd class="faculty-extra-value">
            <?php echo nl2br($wip_detail->education); ?>
          </dd>
    </dt>
  </dl><?php
  endif;
?>
</div><!-- end of left column -->


<!-- start of right column -->
<div class="large-6" style="padding-top:3rem;">

<?php
if (!empty($wip_detail->video)) :?>

<div class="faculty-extra faculty-info text-right">
  <iframe width="480" 
          height="270" 
          src="<?php echo convertYoutube($wip_detail->video)?>" 
          title="YouTube video player" 
          frameborder="0" 
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
          allowfullscreen></iframe>
</div> <!-- end of youtube--> 
<?php
endif
  ?>  
</div> <!-- end of right column -->




</div> <!-- end of faculty-info-container-->

	</div> <!-- end of entry container -->


  </article>
	</main>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>