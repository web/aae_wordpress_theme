
<?php
/**
 * The template to display a single post.
 *
 * Template Name: WebPubs
 */

 /**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */


$pubtypeslug = get_query_var('pubtypeslug');
$title = get_query_var('title');
$is_single_pub = !empty($title);
$pubtype = aae_webpubtype_getbyslug($pubtypeslug);

$pub = null;
$single_pub = null;
$pubs = null;
if ($is_single_pub) {
  $lookup_title = aae_webpub_titleforquery($title);
  $single_pub = aae_webpub_get($lookup_title, $pubtypeslug);
  $pubs = array("0"=>$single_pub);
} else {
  $pubs = aae_webpub_listing_getbyslug($pubtypeslug);
}

//overrides some of the permalinks
function pubs_get_permalink($page) {
  $pubtypeslug = get_query_var('pubtypeslug');

  if ($page == webpubs_listing_pageid()) {
    //get the original slug
    $original_slug = get_post($page)->post_name;
    $original_uri = get_page_uri( $page );
    //replace the original slug with the altered one
    $new_uri = str_replace('/' . $original_slug, '/' . $pubtypeslug, $original_uri);
    $link = home_url($new_uri);
    $link = user_trailingslashit($link, 'page');
    return $link;
    
  } else {
    return get_permalink($page);
  }
}

//creates the pubs breadcrumbs
function aae_pubs_breadcrumbs() {
  	// Settings
	$separator	  = ''; // Using foundation's css solution
	$breadcrums_id	= 'breadcrumbs';
	$breadcrums_class   = 'breadcrumbs';
	$home_title   = 'Home';

	// Get the query & post information
	global $post,$wp_query;

  // Build the breadcrums
  echo '<ul aria-hidden="true" id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

  // Home page
  echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
  //echo '<li class="separator separator-home"> ' . $separator . ' </li>';  

  //assumes already that $post->post_parent is true
  $parent = '';
  // If child page, get parents
  $anc = get_post_ancestors( $post->ID );

  // Get parents in the right order
  $anc = array_reverse($anc);

  // Parent page loop
  foreach ( $anc as $ancestor ) {
    $parent .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . pubs_get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
    //$parent .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
  }

  // Display parent pages
  echo $parent;

  // Current page
  echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';

  //echo home_url('/pubs');
	echo '</ul>';  
}

//custom document title
function webpubs_custom_title($title_parts) {
  $pubtypeslug = get_query_var('pubtypeslug');
  $title = get_query_var('title');
  $title_parts['title'] = webpubs_type_title($pubtypeslug);
  if (!empty($title)) :
    $lookup_title = aae_webpub_titleforquery($title);
    $single_pub = aae_webpub_get($lookup_title, $pubtypeslug);
    $title_parts['title'] = $single_pub->Title . ' - ' . $title_parts['title'];
  endif;

  return $title_parts;
}
add_filter( 'document_title_parts', 'webpubs_custom_title' );

get_header(); ?>

<div id="page" class="content page-builder">
	<main id="main" class="site-main">

	<?php if ( site_uses_breadcrumbs() ) { aae_pubs_breadcrumbs(); } ?>

  <article class=" page type-page hentry">
<?php
  // $first_name = $person->FirstName;
  // $last_name = $person->LastName;
  // $email = $person->Email;
  // $interests = $person->Interests;
  // $office_hours = $person->OfficeHours;
  // $title = $person->Title;



?>

	<div class="entry-content">
    <div class="uw-outer-row row-1 has_text_block default-background">
      <div class="uw-inner-row">
        <div class="uw-column uw-row-header"><?php
        if (!$is_single_pub) :?>
        <h2><?php 
          echo webpubs_type_title($pubtypeslug);
          ;
        ?></h2><?php
        endif; ?></div>
        <div class="uw-column one-column">


<?php 

foreach($pubs as $pub):
if ($pub->ShowOnWeb) :
?>
<div class="row aae-webpub<?php
  if ($is_single_pub) { echo ' aae-webpub-single';}?>">
    <div class="small-12">
        
        <h4><?php
    if (!$is_single_pub) : 
      echo '<a href="/pubs/' . $pubtypeslug . '/' . aae_webpub_titleaslink($pub->Title) . '/">';
    endif;
    echo $pub->Title;
    if (!$is_single_pub) : 
      echo '</a>';
    endif;?></h4>
        <div class="authors">
            <?php 
            
            $authors = explode(PHP_EOL, $pub->Authors);
            foreach($authors as $author) :
                $arr_author = explode(';', $author);
                echo $arr_author[0];
                if (count($arr_author) > 1):
                    echo ' [<a href="mailto://' . trim($arr_author[1]) . '">' . trim($arr_author[1]) . '</a>]';
                endif;
                echo '<br />';
            endforeach;
            ?>
            
        </div>
        <div class="series-details"><?php echo $pubtype->Title . ' No. ' . $pub->SeriesNo . ', ';
        if ($pub->IsRevised) :
          echo 'Revised: ';
        endif;
        echo date('F Y', strtotime($pub->Date)) . ', ' . $pub->Pages . 'p.';?></div>
        <?php
  if ($is_single_pub) :
    if ( !empty($pub->Abstract)):
?>  <div class="abstract"><?php echo $pub->Abstract;?></div><?php
    endif;

    if ($pub->HavePDF or count($pub->Files) > 0) :
      ?><div class="files"><?php
      if ($pub->HavePDF) :
        echo '<a href="' . aae_webpub_pdflink($pub, $pubtype) . '">Read Full Text</a><br />';
      endif;
      foreach($pub->Files as $file):
        echo '<a href="' . aae_webpub_basedownload_link($pubtypeslug) . '/' . trim($file->Filename) . '">';
        echo !empty($file->Description) ? $file->Description : $file->Filename;
        echo '</a><br />';
      endforeach;
      ?></div><?php
    endif;
  endif;?>
    </div>
</div>

<?php
endif;
endforeach;  //end of pubs foreach

?>


  </div></div></div><!-- end of uw-outerrow, inner-row, one-column-->
	</div> <!-- end of entry container -->


  </article>
	</main>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>