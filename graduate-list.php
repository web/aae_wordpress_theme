
<?php
/**
 * The template to display a single post.
 *
 * Template Name: Graduate Listing
 */

 /**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
 

$uwterm = get_query_var('uwterm');
$grad_listing_term = null;
  
if (!$uwterm) {
  $grad_listing_term = get_uw_term_bycode(graduate_latest_uwterm_code());
} else {
  $grad_listing_term = get_uw_term_bycode($uwterm);
}

$graduate_list = graduate_listing($grad_listing_term);
if (count($graduate_list) < 1) {
    force_404();
}


// now put the graduates in a grouped array for each level (Undergrad, Master's, PhD, etc)
$grouped_graduates = [];
$graduate_group = null;
foreach($graduate_list as $graduate) :
    if (empty($graduate_group) or $graduate_group->level != $graduate->levelDescription) {
        // the current graduate_group is not the same level as this graduate. 
        // create a new graduate_group with this level, and push it onto the grouped_graduates
        $graduate_group = new stdClass;
        $graduate_group->level = $graduate->levelDescription;
        $graduate_group->graduates = [];
        array_push($grouped_graduates, $graduate_group);
    }

    // push the graduate onto the current graduate_group
    array_push($graduate_group->graduates, $graduate);
endforeach;

function graduatelist_custom_title($title_parts) {
    $uwterm = get_query_var('uwterm');
    $grad_listing_term = null;
      
    if (!$uwterm) {
      $grad_listing_term = get_uw_term_bycode(graduate_latest_uwterm_code());
    } else {
      $grad_listing_term = get_uw_term_bycode($uwterm);
    }

     $title_parts['title'] = $grad_listing_term->shortDescription . ' Graduates';

     return $title_parts;
}
add_filter( 'document_title_parts', 'graduatelist_custom_title' );

function graduatelist_custom_breadcrumb( $title, $id = null ) {

	if ($id == graduate_list_pageid()) {
		return graduatelist_custom_breadcrumb_actual($title, $id);
	}
  
	return $title;
}
add_filter( 'the_title', 'graduatelist_custom_breadcrumb', 10, 2 );

get_header(); ?>

<div id="page" class="content page-builder">
	<main id="main" class="site-main">

	<?php if ( site_uses_breadcrumbs() ) { custom_breadcrumbs(); } ?>

  <article class=" page type-page hentry">
<?php

?>

<header class="entry-header">
		<h1 class="page-title uw-mini-bar"><?php echo $grad_listing_term->shortDescription;?> Graduates</h1>	</header>
	<div class="entry-content">
    <div class="uw-outer-row row-1 has_text_block default-background"><div class="uw-inner-row"><div class="uw-column one-column">
<?php
$one_column_layout = false;
$display_photos = true;
$image_size = 'Custom';
$custom_width = 240;
$custom_height = 240;
$last_to_first = false;
$show_degree = true;
$show_hometown = true;
$foundation_grid_cols = 3;  // 3 for 4 col, 4 for 3 col, 6 for 2 col

foreach($grouped_graduates as $grp_graduates):
    ?><h2><?php echo $grp_graduates->level;?></h2>
    <div class="faculty-list">
<?php
     foreach($grp_graduates->graduates as $graduate) :
?>
<div class="faculty-member column small-12 medium-<?php echo $foundation_grid_cols ?>">
	<div class="faculty-member-content">

	<?php
	if ( $one_column_layout ) :
		echo '<div class="row">';
		if ( $display_photos ) :
			echo '<div class="column shrink">';
		endif;
	endif;

	if($display_photos) :
		if ( $image_size == 'Thumbnail (site default)') :
			$image_src = 'thumbnail';
			$image_class = 'thumbnail';
		elseif ($image_size == 'Custom') :
			$image_src = array($custom_width, $custom_height);
			$image_class = 'custom';
		else :
			$image_src = 'uw-headshot';
			$image_class = 'default';
		endif;
	?>
		<div
			class="faculty-image<?php echo ' ' . $image_class; ?>"
			<?php if($image_size == 'Custom') : //set custom dimensions as max-width and max-height ?>
				style="max-width: <?php echo $custom_width; ?>px; max-height: <?php echo $custom_height; ?>px;"
			<?php endif; ?>
			>
			<a href="<?php echo get_graduate_link($graduate, $grad_listing_term) ?>" tabindex="-1" aria-hidden="true">
				<?php
				if ( property_exists($graduate, 'photoUrl') && $graduate->photoUrl != '' ) :
					$img_size = array('width'=>$custom_width,'height'=>$custom_height);

					echo graduate_image_tag($graduate->photoUrl, $graduate->firstName . ' ' . $graduate->lastName, $grad_listing_term, $img_size, 320);
				else : ?>
					<img class="buckyhead" src="<?php  echo get_stylesheet_directory_uri() . '/dist/img/no-photo.png'; ?>"/>
				<?php endif; ?>
			</a>
		</div>
	<?php endif; //end if($display_photos) ?>

	<?php
	if ( $one_column_layout ) :
		if($display_photos) :
			echo '</div>';
		endif;
		echo '<div class="column">';
	endif;
	?>

		<h3><a href="<?php echo get_graduate_link($graduate, $grad_listing_term); ?>">
			<?php echo $last_to_first ? $graduate->lastName. ', ' . $graduate->firstName. ' ' . $graduate->middleName : $graduate->firstName . ' ' . $graduate->middleName . ' ' . $graduate->lastName ?>
		</a></h3>
		<?php echo ($show_degree and !empty($graduate->degree)) ? '<p>' . str_replace("\r\n", '<br />', $graduate->degree) . '</p>' : ''; ?>

		<?php echo ($show_hometown and (!empty($graduate->hometownCity) and !empty($graduate->hometownState))) ? '<p class="hometown">' . $graduate->hometownCity . ', ' . $graduate->hometownState . '</p>' : '' ?>

<?php
	if ($one_column_layout ) :
		echo '</div></div>';
	endif;
	?>
	</div>
</div>

<?php endforeach; // end of a single group (level) of graduates  ?>

    </div><!-- end of faculty-list -->
<?php endforeach; wp_reset_postdata(); // end of grouped graduates />
?>
  </div></div></div><!-- end of uw-outerrow, inner-row, one-column-->
	</div> <!-- end of entry container -->


  </article>
	</main>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>
