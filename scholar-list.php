
<?php
/**
 * The template to display a single post.
 *
 * Template Name: Scholar Listing
 */

 /**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
 

$uwterm = get_query_var('uwterm');
$scholar_listing_term = null;
  
if (!$uwterm) {
  $scholar_listing_term = get_uw_term_bycode(scholar_latest_uwterm_code());
} else {
  $scholar_listing_term = get_uw_term_bycode($uwterm);
}

$scholar_list = scholar_listing($scholar_listing_term);

if (count($scholar_list) < 1) {
    force_404();
}

// now put the scholars in a grouped array for each level (Undergrad, Graduate, etc)
$grouped_scholars = [];
$scholar_group = null;
foreach($scholar_list as $scholar) :
    if (empty($scholar_group) or $scholar_group->level != $scholar->levelDescription) {
        // the current scholar_group is not the same level as this scholar. 
        // create a new scholar_group with this level, and push it onto the grouped_scholars
        $scholar_group = new stdClass;
        $scholar_group->level = $scholar->levelDescription;
        $scholar_group->scholars = [];
        array_push($grouped_scholars, $scholar_group);
    }

    // push the scholar onto the current scholar_group
    array_push($scholar_group->scholars, $scholar);
endforeach;

function scholarlist_custom_title($title_parts) {
    $uwterm = get_query_var('uwterm');
    $scholar_listing_term = null;
      
    if (!$uwterm) {
      $scholar_listing_term = get_uw_term_bycode(scholar_latest_uwterm_code());
    } else {
      $scholar_listing_term = get_uw_term_bycode($uwterm);
    }

     $title_parts['title'] = $scholar_listing_term->shortDescription . ' Scholars &amp; Sponsors';

     return $title_parts;
}
add_filter( 'document_title_parts', 'scholarlist_custom_title' );

function scholarlist_custom_breadcrumb( $title, $id = null ) {

	if ($id == scholar_list_pageid()) {
		return scholarlist_custom_breadcrumb_actual($title, $id);
	}
  
	return $title;
}
add_filter( 'the_title', 'scholarlist_custom_breadcrumb', 10, 2 );

get_header(); ?>

<div id="page" class="content page-builder">
	<main id="main" class="site-main">

	<?php if ( site_uses_breadcrumbs() ) { custom_breadcrumbs(); } ?>

  <article class=" page type-page hentry">
<?php

?>

<header class="entry-header">
		<h1 class="page-title uw-mini-bar"><?php echo $scholar_listing_term->shortDescription;?> Scholars &amp; Sponsors</h1>	</header>
	<div class="entry-content">
    <div class="uw-outer-row row-1 has_text_block default-background"><div class="uw-inner-row"><div class="uw-column one-column">
<?php
$one_column_layout = false;
$display_photos = true;
$image_size = 'Custom';
$custom_width = 240;
$custom_height = 240;
$last_to_first = false;
$show_degree = true;
$show_hometown = true;
$foundation_grid_cols = 3;  // 3 for 4 col, 4 for 3 col, 6 for 2 col

foreach($grouped_scholars as $grp_scholars):
    ?><h2><?php echo $grp_scholars->level;?></h2>
    <div class="faculty-list">
<?php
     foreach($grp_scholars->scholars as $scholar) :
?>
<div class="faculty-member column small-12 medium-<?php echo $foundation_grid_cols ?>">
	<div class="faculty-member-content">

	<?php
	if ( $one_column_layout ) :
		echo '<div class="row">';
		if ( $display_photos ) :
			echo '<div class="column shrink">';
		endif;
	endif;

	if ( $one_column_layout ) :
		if($display_photos) :
			echo '</div>';
		endif;
		echo '<div class="column">';
	endif;
	?>

		<h3><a href="<?php echo get_scholar_link($scholar, $scholar_listing_term); ?>">
			<?php echo $last_to_first ? $scholar->lastName. ', ' . $scholar->firstName. ' ' . $scholar->middleName : $scholar->firstName . ' ' . $scholar->middleName . ' ' . $scholar->lastName ?>
		</a></h3>
		<?php echo ($show_degree and !empty($scholar->degree)) ? '<p>' . str_replace("\r\n", '<br />', $scholar->degree) . '</p>' : ''; ?>

		<?php echo ($show_hometown and (!empty($scholar->hometownCity) and !empty($scholar->hometownState))) ? '<p class="hometown">' . $scholar->hometownCity . ', ' . $scholar->hometownState . '</p>' : '' ?>

<?php
	if ($one_column_layout ) :
		echo '</div></div>';
	endif;
	?>
	</div>
</div>

<?php endforeach; // end of a single group (level) of graduates  ?>

    </div><!-- end of faculty-list -->
<?php endforeach; wp_reset_postdata(); // end of grouped graduates />
?>
  </div></div></div><!-- end of uw-outerrow, inner-row, one-column-->
	</div> <!-- end of entry container -->


  </article>
	</main>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>
