<?php
/**
 * The template to display a single post.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package UW Theme
 */

get_header();

if ( site_uses_breadcrumbs() ) { custom_breadcrumbs(); } ?>

<div id="page" class="content">
	<main id="main" class="site-main">
		<?php

		// Start the loop.
		while ( have_posts() ) : the_post();
			get_template_part( 'content-parts/content', 'single' );


			// End of the loop.
		endwhile;
		?>

	</main>


</div>

<?php get_footer(); ?>
